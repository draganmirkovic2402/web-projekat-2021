package beans;

public class PostContentBean {
	private String path;
	private String caption;
	private boolean isPost;
	
	public PostContentBean() {}

	public PostContentBean(String path, String caption, boolean isPost) {
		super();
		this.path = path;
		this.caption = caption;
		this.isPost = isPost;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public boolean isPost() {
		return isPost;
	}

	public void setPost(boolean isPost) {
		this.isPost = isPost;
	}

	@Override
	public String toString() {
		return "PostContentBean [path=" + path + ", caption=" + caption + ", isPost=" + isPost + "]";
	}
	
	

}
