package beans;

public class PostCommentBean {
	private int postId;
	private String text;
	
	public PostCommentBean() {}
	
	public PostCommentBean(int postId, String text) {
		super();
		this.postId = postId;
		this.text = text;
	}


	public int getPostId() {
		return postId;
	}


	public void setPostId(int postId) {
		this.postId = postId;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "PostCommentBean [postId=" + postId + ", text=" + text + "]";
	}
	
	

}
