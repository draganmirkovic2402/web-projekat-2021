package beans;

public class RequestHandlerBean {
	private int id;
	private boolean accept;
	
	public RequestHandlerBean() {}
	
	
	
	public RequestHandlerBean(int id, boolean accept) {
		super();
		this.id = id;
		this.accept = accept;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isAccept() {
		return accept;
	}
	public void setAccept(boolean accept) {
		this.accept = accept;
	}



	@Override
	public String toString() {
		return "RequestHandlerBean [id=" + id + ", accept=" + accept + "]";
	}
	
	
	
}
