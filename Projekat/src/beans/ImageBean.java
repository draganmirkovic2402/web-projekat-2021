package beans;

import java.util.Collection;

public class ImageBean {
	private int id;
	private String picturePath;
	private String date;
	private String caption;
	private Collection<CommentBean> comments;
	
	
	public ImageBean() {}


	public ImageBean(int id, String picturePath, String date, String caption, Collection<CommentBean> comments) {
		super();
		this.id = id;
		this.picturePath = picturePath;
		this.date = date;
		this.caption = caption;
		this.comments = comments;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getPicturePath() {
		return picturePath;
	}


	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getCaption() {
		return caption;
	}


	public void setCaption(String caption) {
		this.caption = caption;
	}


	public Collection<CommentBean> getComments() {
		return comments;
	}


	public void setComments(Collection<CommentBean> comments) {
		this.comments = comments;
	}


	@Override
	public String toString() {
		return "ImageBean [id=" + id + ", picturePath=" + picturePath + ", date=" + date + ", caption=" + caption
				+ ", comments=" + comments + "]";
	}


	
	
	
	
	
}
