package beans;

import java.util.Date;

public class SignUpBean {
	private String name;
	private String lastname;
	private String email;
	private String username;
	private String password;
	private String passwordConfirm;
	private String dob;
	private String gender;
	
	public SignUpBean() {}

	public SignUpBean(String name, String lastname, String email, String username, String password,
			String passwordConfirm, String dob, String gender) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.email = email;
		this.username = username;
		this.password = password;
		this.passwordConfirm = passwordConfirm;
		this.dob = dob;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "SignUpBean [name=" + name + ", lastname=" + lastname + ", email=" + email + ", username=" + username
				+ ", password=" + password + ", passwordConfirm=" + passwordConfirm + ", dob=" + dob + ", gender="
				+ gender + "]";
	}
	
	

}
