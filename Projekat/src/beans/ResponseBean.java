package beans;

/**
Klasa zamisljena kao odgovor na zahtjev
Message - poruka za ispis proizvoljna
Code - kod odgovora
	1 - uspijesno
	2 - ne postoji taj username
	3 - pogresna sifra
	4 - Invalidno ime
*/

public class ResponseBean {
	private String message;
	private int code;
	
	public ResponseBean() {}

	public ResponseBean(String message, int code) {
		super();
		this.message = message;
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "ResponseBean [message=" + message + ", code=" + code + "]";
	}
	
	
	
	

}
