package beans;

public class UsernameBean {
	private String username;
	
	public UsernameBean() {}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UsernameBean(String username) {
		super();
		this.username = username;
	}

	@Override
	public String toString() {
		return "UsernameBean [username=" + username + "]";
	}
	
	
}
