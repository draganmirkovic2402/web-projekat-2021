package beans;

public class UserEditBean {
	private String picturePath;
	private String name;
	private String lastname;
	private String email;
	private String password;
	private String passwordConfirm;
	private String dob;
	private String gender;
	private boolean isPrivate;
	
	
	public UserEditBean() {}
	
	

	public UserEditBean(String picturePath,String name, String lastname, String email, String password,String passwordConfirm, String dob,String gender,boolean isPrivate) {
		super();
		this.picturePath=picturePath;
		this.name = name;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.passwordConfirm=passwordConfirm;
		this.dob = dob;
		this.gender=gender;
		this.isPrivate=isPrivate;
		
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	


	public String getPicturePath() {
		return picturePath;
	}



	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}



	public String getPasswordConfirm() {
		return passwordConfirm;
	}



	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public boolean getIsPrivate() {
		return isPrivate;
	}



	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}



	




	@Override
	public String toString() {
		return "UserEditBean [picturePath=" + picturePath + ", name=" + name + ", lastname=" + lastname + ", email="
				+ email + ", password=" + password + ", passwordConfirm=" + passwordConfirm + ", dob=" + dob
				+ ", gender=" + gender + ", isPrivate=" + isPrivate +  "]";
	}




}
