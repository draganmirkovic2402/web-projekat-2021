package beans;

public class SearchBean {
	private String searchName;
	private String searchLastname;
	private String dobFrom;
	private String dobTo;
	private int sortCrit;
	private int sortType;
	
	public SearchBean() {}

	
	public SearchBean(String searchName, String searchLastname, String dobFrom, String dobTo, int sortCrit,
			int sortType) {
		super();
		this.searchName = searchName;
		this.searchLastname = searchLastname;
		this.dobFrom = dobFrom;
		this.dobTo = dobTo;
		this.sortCrit = sortCrit;
		this.sortType = sortType;
	}


	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getSearchLastname() {
		return searchLastname;
	}

	public void setSearchLastname(String searchLastname) {
		this.searchLastname = searchLastname;
	}

	public String getDobFrom() {
		return dobFrom;
	}

	public void setDobFrom(String dobFrom) {
		this.dobFrom = dobFrom;
	}

	public String getDobTo() {
		return dobTo;
	}

	public void setDobTo(String dobTo) {
		this.dobTo = dobTo;
	}

	public int getSortCrit() {
		return sortCrit;
	}

	public void setSortCrit(int sortCrit) {
		this.sortCrit = sortCrit;
	}

	public int getSortType() {
		return sortType;
	}

	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

	@Override
	public String toString() {
		return "SearchBean [searchName=" + searchName + ", searchLastname=" + searchLastname + ", dobFrom=" + dobFrom
				+ ", dobTo=" + dobTo + ", sortCrit=" + sortCrit + ", sortType=" + sortType + "]";
	}
	
	

}
