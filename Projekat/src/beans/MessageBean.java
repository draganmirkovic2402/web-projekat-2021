package beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageBean implements Comparable<MessageBean>{
	private int reciever;
	private String text;
	private String date;
	private boolean isFromSender;
	
	public MessageBean() {};
	public MessageBean(int reciever,String text, String date,boolean isFromSender) {
		super();
		this.reciever=reciever;
		this.text = text;
		this.date = date;
		this.isFromSender=isFromSender;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getdate() {
		return date;
	}
	public void setdate(String date) {
		this.date = date;
	}
	
	
	
	public boolean isFromSender() {
		return isFromSender;
	}
	public void setFromSender(boolean isFromSender) {
		this.isFromSender = isFromSender;
	}
	@Override
	public String toString() {
		return "MessageBean [text=" + text + ", date=" + date + ", isFromSender=" + isFromSender + "]";
	}
	
	private Date getDate(String date) throws ParseException {
		  DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    formatter.setLenient(false);
		    return formatter.parse(date);
	}

	
	
	
	
	public int getReciever() {
		return reciever;
	}
	public void setReciever(int reciever) {
		this.reciever = reciever;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public int compareTo(MessageBean o) {
		try {
			return getDate(date).compareTo(getDate(o.getdate()));
		} catch (ParseException e) {
			return 0;
			
		}
	}

	
	
}
