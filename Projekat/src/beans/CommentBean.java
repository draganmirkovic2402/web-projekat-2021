package beans;

import java.util.Date;

public class CommentBean {
	private int id;
	private String username;
	private String name;
	private String lastname;
	private String text;
	private String date;
	private boolean isEdited;
	
	public CommentBean() {}

	public CommentBean(int id, String username,String name, String lastname, String text, String date, boolean isEdited) {
		super();
		this.id = id;
		this.username=username;
		this.name = name;
		this.lastname = lastname;
		this.text = text;
		this.date = date;
		this.isEdited = isEdited;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}

	@Override
	public String toString() {
		return "CommentBean [id=" + id + ", username=" + username + ", name=" + name + ", lastname=" + lastname
				+ ", text=" + text + ", date=" + date + ", isEdited=" + isEdited + "]";
	}




	
	

	
}
