package beans;

import java.io.Serializable;

public class TextBean{
	/**
	 * 
	 */
	
	private int senderId;
	private int recieverId;
	private String text;
	
	public TextBean() {}

	public TextBean(int senderId, int recieverId, String text) {
		super();
		this.senderId = senderId;
		this.recieverId = recieverId;
		this.text = text;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getRecieverId() {
		return recieverId;
	}

	public void setRecieverId(int recieverId) {
		this.recieverId = recieverId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}



	@Override
	public String toString() {
		return "TextBean [senderId=" + senderId + ", recieverId=" + recieverId + ", text=" + text + "]";
	}

	


}
