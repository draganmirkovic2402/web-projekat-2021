package beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserTableBean implements Comparable<UserTableBean>{
	private String picturePath;
	private String name;
	private String lastname;
	private String username;
	private String email;
	private String dob;
	private String gender;
	private boolean isPrivate;
	private boolean isAdmin;
	private boolean isBlocked;
	
	public UserTableBean() {}

	public UserTableBean(String picturePath, String name, String lastname, String username, String email, String dob,String gender,boolean isPrivate,boolean isAdmin,boolean isBlocked) {
		super();
		this.picturePath = picturePath;
		this.name = name;
		this.lastname = lastname;
		this.username = username;
		this.email = email;
		this.dob = dob;
		this.gender=gender;
		this.isPrivate=isPrivate;
		this.isAdmin=isAdmin;
		this.isBlocked=isBlocked;
		
	}


	
	

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "UserTableBean [picturePath=" + picturePath + ", name=" + name + ", lastname=" + lastname + ", username="
				+ username + ", email=" + email + ", dob=" + dob + ", gender=" + gender + ", isPrivate=" + isPrivate
				+ ", isAdmin=" + isAdmin + ", isBlocked=" + isBlocked + "]";
	}

	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
	
	
	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	private Date getDate(String dob) throws ParseException {
		  DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    formatter.setLenient(false);
		    return formatter.parse(dob);
	}

	@Override
	public int compareTo(UserTableBean o) {
		try {
			return getDate(dob).compareTo(getDate(o.getDob()));
		} catch (ParseException e) {
			return 0;
			
		}
	}
	
	

}
