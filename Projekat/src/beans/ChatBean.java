package beans;

import java.util.Collection;

public class ChatBean {
	private int id; //sender
	private int reciever;
	private String picturePath;
	private String name;
	private String lastname;
	private boolean isAdmin;
	private Collection<MessageBean> messages;
	
	public ChatBean() {};
	
	public ChatBean(int id, int rereciever,String picturePath,String name, String lastname, boolean isAdmin, Collection<MessageBean> messages) {
		super();
		this.id = id;
		this.reciever=reciever;
		this.picturePath=picturePath;
		this.name = name;
		this.lastname = lastname;
		this.isAdmin = isAdmin;
		this.messages = messages;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getlastname() {
		return lastname;
	}

	public void setlastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Collection<MessageBean> getMessages() {
		return messages;
	}

	public void setMessages(Collection<MessageBean> messages) {
		this.messages = messages;
	}

	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	

	public int getReciever() {
		return reciever;
	}

	public void setReciever(int reciever) {
		this.reciever = reciever;
	}

	@Override
	public String toString() {
		return "ChatBean [id=" + id + ", picturePath=" + picturePath + ", name=" + name + ", lastname=" + lastname
				+ ", isAdmin=" + isAdmin + ", messages=" + messages + "]";
	}

	
	
	
	
}
