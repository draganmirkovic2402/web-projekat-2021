package beans;

import model.Status;

public class FriendRequestBean {
	private int id;
	private String name;
	private String lastname;
	private String Date;
	private Status status;
	
	public FriendRequestBean() {};
	
	public FriendRequestBean(int id, String name, String lastname,String date, Status status) {
		super();
		this.id = id;
		this.name = name;
		this.lastname=lastname;
		Date = date;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "FriendRequestBean [id=" + id + ", name=" + name + ", lastname="+lastname+", Date=" + Date + ", status=" + status + "]";
	}
	
	
	
}
