package rest;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFiles;
import static spark.Spark.webSocket;

import java.io.Console;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.server.ResponseWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import beans.ChatBean;
import beans.CommentBean;
import beans.ImageBean;
import beans.LoginBean;
import beans.MessageBean;
import beans.PostBean;
import beans.PostCommentBean;
import beans.PostContentBean;
import beans.RequestHandlerBean;
import beans.ResponseBean;
import beans.SearchBean;
import beans.SignUpBean;
import beans.TextBean;
import beans.UserEditBean;
import beans.UserTableBean;
import beans.UsernameBean;
import dao.MainDao;
import dao.UserDao;
import freemarker.template.Configuration;
import freemarker.template.Template;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import model.Comment;
import model.DirectMessage;
import model.FriendRequest;
import model.Image;
import model.Post;
import model.User;
import spark.Response;
import spark.Session;
import spark.TemplateEngine;
import ws.WsHandler;


public class SparkAppMain {
	public static MainDao mainDao;
	private static Gson g = new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.FULL).create();
	static Map<Session,String> userUsernameMap=new ConcurrentHashMap<Session, String>();
	

	public static void main(String[] args) throws IOException {
		System.out.println("Server starting ...");
		mainDao=new MainDao();
		//mainDao.createMockUpData(true);
		boolean status=mainDao.loadData();
		
		
		if (status) 
			System.out.println("Data successfully loaded ...");
		else
			System.out.println("Data unsuccessfully loaded ...");
			
			
			
			
		port(8080);

		webSocket("/ws", WsHandler.class);

		staticFiles.externalLocation(new File("./static").getCanonicalPath());
		
		initFtl();
		
		System.out.println("Server running on port 8080 ...");
		
		get("/rest/demo/test", (req, res) -> {
			return "Works";
		});
		
		
		post("/rest/login", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			
			LoginBean u = g.fromJson(payload, LoginBean.class);
			
			ResponseBean rsp=mainDao.userDao.login(u);	
			Session ss=req.session(true);
			User logged=ss.attribute("user");
			if(logged==null) {
				if (rsp.getCode()==1) {
				ss.attribute("user",mainDao.userDao.getUserByUsername(u.getUsername()));
			}
			}else {
				res.status(404);
				res.body("<b>404 Not found: TEST </b> ");
				res.type("text/html");		
				rsp.setMessage("User already logged.");
				rsp.setCode(-1);
			}
			
			return g.toJson(rsp);
		});
		
	
		
		get("/rest/indexLogout", (req, res) -> {
			Session ss = req.session(true);
			ss.attribute("user",null);
			return null;
		});
		
		get("/rest/logout", (req, res) -> {
			Session ss = req.session(true);
			ss.attribute("user",null);
			res.redirect("../index.html");
			
			return null;
		});
		
		
		
		
		post("/rest/register", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
	
			SignUpBean u = g.fromJson(payload, SignUpBean.class);
			

			ResponseBean rsp=mainDao.userDao.signUp(u);	
			
			if(rsp.getCode()!=1) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/register "+rsp.getMessage()+" </b> ");
				res.type("text/html");		
			}
			
			return g.toJson(rsp);
		});
		
		
		get("rest/getFriends", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");
		
			return g.toJson(mainDao.userDao.getUserFriendsTable(user));
		});
		
		
		
		get("rest/getOtherUsers", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");
		
			return g.toJson(mainDao.userDao.getOtherUserTable(user));
		});
		
		
		
		post("rest/getFilteredOtherUsers", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");
			String payload = req.body();
			
			SearchBean searchBean = g.fromJson(payload, SearchBean.class);
			
			return g.toJson(mainDao.userDao.getFilteredOtherUsers(user,searchBean));
		});
		
		get("rest/getLoggedUserInfo", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");	
			if(user==null) {
				
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getLoggedUserInfo User not logged </b> ");
				res.type("text/html");
				return null;
			
		}
			return g.toJson(mainDao.userDao.getUserInfo(user));
			
		});
		
		
		get("rest/getLoggedUserEditData", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");		
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getLoggedUserInfo User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			return g.toJson(mainDao.userDao.getUserEditData(user));
			
		});
			
		
		
		get("rest/getLoggedUserId", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");		
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getLoggedUserId User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			return g.toJson(user.getId());
			
		});
		
		get("rest/isLogged", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");
		
			boolean logged;
			
			if (user == null) {
				logged=false;
				
				return g.toJson(logged);  
			} else {
				logged=true;
				return g.toJson(logged);
			}
		});
		
		
		get("rest/getIsMine", (req, res) -> {
			Session ss = req.session(true);
			User user=ss.attribute("user");
			res.type("application/json");
			User userView=ss.attribute("userView");
			boolean logged;
			
			if (user == null) {
				logged=false;
				
				return g.toJson(logged);  
			} else {
				if(user.getId()==userView.getId()) {
					logged=true;
					return g.toJson(logged);
				}
				logged=false;
				return g.toJson(logged);
			}
		});
		
		post("/rest/saveData", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");

			UserEditBean u = g.fromJson(payload, UserEditBean.class);
			
			ResponseBean rsp=mainDao.userDao.editUserData(u,user.getUsername());
			if(rsp.getCode()!=1) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/saveData "+rsp.getMessage()+" </b> ");
				res.type("text/html");		
			}
			
			return g.toJson(rsp);
		});
		
		
		
		post("/rest/makePublic", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			ResponseBean rsp=mainDao.userDao.makePublic(user,true);
			if(rsp.getCode()!=1) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/makePublic "+rsp.getMessage()+" </b> ");
				res.type("text/html");		
			}
			return g.toJson(rsp);
		});
		
		post("/rest/makePrivate", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			ResponseBean rsp=mainDao.userDao.makePublic(user,false);
			if(rsp.getCode()!=1) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/makePrivate "+rsp.getMessage()+" </b> ");
				res.type("text/html");		
			}			
			return g.toJson(rsp);
		});
		
		post("/rest/removeFriend", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");

			UsernameBean u = g.fromJson(payload, UsernameBean.class);
		
			ResponseBean rsp=mainDao.userDao.removeFriend(user.getUsername(),u.getUsername());
			if(rsp.getCode()!=1) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/removeFriend "+rsp.getMessage()+" </b> ");
				res.type("text/html");		
			}
			return g.toJson(rsp);
		});
		
		
		post("/rest/handleRequest", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");

			RequestHandlerBean reqHandler = g.fromJson(payload, RequestHandlerBean.class);
			
			ResponseBean rsp=mainDao.userDao.handleRequest(user,reqHandler);
			if(rsp.getCode()!=1) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/handleRequest "+rsp.getMessage()+" </b> ");
				res.type("text/html");		
			}	
			return g.toJson(rsp);
		});
		
		post("/rest/addFriend", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getUserFriendRequests User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			UsernameBean u = g.fromJson(payload, UsernameBean.class);
		
			ResponseBean rsp=mainDao.userDao.addFriend(user.getUsername(),u.getUsername());
			
			return g.toJson(rsp);
		});
		
		
		get("/rest/getUserFriendRequests", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");			
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getUserFriendRequests User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			return g.toJson(mainDao.userDao.getUserFriendRequests(user));
		
		});
		
		
		
		
		get("/rest/getChats", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");			
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getChats User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			return g.toJson(mainDao.userDao.getUserChats(user));
		
		});
		
		post("/rest/sendMessage", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/sendMessage User not logged </b> ");
				res.type("text/html");	
				return null;
			}
		
			
			TextBean txtBean = g.fromJson(payload, TextBean.class);
			
			
			MessageBean msgBean=mainDao.userDao.sendMessage(user,txtBean);			
			return g.toJson(msgBean);
		});
		
		post("/rest/sendMessageUser", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
		
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/sendMessageUser User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			
			
			TextBean txtBean = g.fromJson(payload, TextBean.class);
			
			
			MessageBean msgBean=mainDao.userDao.getUserMessageBean(user,txtBean);
			
			
			return g.toJson(msgBean);
		});
		
		post("/rest/postComment", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");

			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/postComment User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			
			
			PostCommentBean PostcmntBean = g.fromJson(payload, PostCommentBean.class);
			
			CommentBean cmntBean=mainDao.userDao.postComment(user,PostcmntBean);
			return g.toJson(cmntBean);
		});
		
		post("/rest/imageComment", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/imageComment User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			PostCommentBean PostcmntBean = g.fromJson(payload, PostCommentBean.class);  // {id , text } id from image not post !!!
			
			CommentBean cmntBean=mainDao.userDao.postCommentImage(user,PostcmntBean);
			return g.toJson(cmntBean);
		});
		
		
	
		
		post("/rest/setAsProfilePicture", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/setAsProfilePicture User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			PostCommentBean PostcmntBean = g.fromJson(payload, PostCommentBean.class);  // {id , text } id from image not post !!!
			
			ResponseBean rspBean=mainDao.userDao.setUserProfilePicture(user,PostcmntBean.getPostId());
			return g.toJson(rspBean);
		});
		
		post("/rest/getMutualFriends", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getMutualFriends User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			
			UsernameBean userBean = g.fromJson(payload, UsernameBean.class);
			
			return g.toJson(mainDao.userDao.getMutualFriends(user.getUsername(),userBean.getUsername()));
				
		
		});
		
		
		
		
		post("/rest/setViewUser", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			

			UsernameBean userBean = g.fromJson(payload, UsernameBean.class);
			User userView=mainDao.userDao.getUserByUsername(userBean.getUsername());
			
			if(userView==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/setViewUser User not selected </b> ");
				res.type("text/html");	
				return null;
			}
			ss.attribute("userView", userView);
			ResponseBean rsp=new ResponseBean("Set", 1);
			return g.toJson(rsp);
				
		
		});
		
		post("/rest/deletePost", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/deletePost User not logged </b> ");
				res.type("text/html");	
				return null;
			}

			PostBean postBean = g.fromJson(payload, PostBean.class);
			
			ResponseBean rsp=mainDao.userDao.deletePost(postBean);
			return g.toJson(rsp);
				
		
		});
		
		
		
		post("/rest/deleteImage", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/deleteImage User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			ImageBean imgBean = g.fromJson(payload, ImageBean.class);
			
			ResponseBean rsp=mainDao.userDao.deleteImage(imgBean);
			return g.toJson(rsp);
				
		
		});
	
		post("/rest/deleteComment", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/deleteComment User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			PostCommentBean postBean = g.fromJson(payload, PostCommentBean.class);
			
			ResponseBean rsp=mainDao.userDao.deleteComment(postBean);
			return g.toJson(rsp);
				
		
		});
		
		
		
		post("/rest/adminPostDelete", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/adminPostDelete User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			if(user.isAdmin()==false) {
				res.status(401);
				res.header("Unauthorized","401");
				res.body("<b>401 Unauthorized: /rest/adminPostDelete User is not admin </b> ");
				res.type("text/html");	
				return null;
			}
			
			User userView=ss.attribute("userView");
			if(userView==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/adminPostDelete User not selected </b> ");
				res.type("text/html");	
				return null;
			}
			PostCommentBean postBean = g.fromJson(payload, PostCommentBean.class); //postId and a reason
			
			ResponseBean rsp=mainDao.userDao.adminDeletePost(postBean,userView.getId(),user.getId());
			return g.toJson(rsp);
				
		
		});
		
	
		
		
		post("/rest/blockUser", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/blockUser User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			if(user.isAdmin()==false) {
				res.status(401);
				res.header("Unauthorized","401");
				res.body("<b>401 Unauthorized: /rest/blockUser User is not admin </b> ");
				res.type("text/html");	
				return null;
			}
			UserTableBean userBean = g.fromJson(payload, UserTableBean.class); //postId and a reason
			User userToBlock= mainDao.userDao.getUserByUsername(userBean.getUsername());
			ResponseBean rsp=new ResponseBean();
			if (userToBlock==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/blockUser User not selected </b> ");
				res.type("text/html");	
				rsp.setCode(-1);
				rsp.setMessage("Error");
				return g.toJson(rsp);
			}
			
			rsp=mainDao.userDao.blockUser(userToBlock);
			return g.toJson(rsp);
				
		
		});
		
		
		
	
		
		post("/rest/unblockUser", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/unblockUser User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			if(user.isAdmin()==false) {
				res.status(401);
				res.header("Unauthorized","401");
				res.body("<b>401 Unauthorized: /rest/unblockUser User is not admin </b> ");
				res.type("text/html");	
				return null;
			}
			
			UserTableBean userBean = g.fromJson(payload, UserTableBean.class); //postId and a reason
			User userToBlock= mainDao.userDao.getUserByUsername(userBean.getUsername());
			ResponseBean rsp=new ResponseBean();
			if (userToBlock==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/unblockUser User not selected </b> ");
				res.type("text/html");	
				rsp.setCode(-1);
				rsp.setMessage("Error");
				return g.toJson(rsp);
			}
			
			rsp=mainDao.userDao.unblockUser(userToBlock);
			return g.toJson(rsp);
				
		
		});
		
		post("/rest/adminImageDelete", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			User userView=ss.attribute("userView");
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/adminImageDelete User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			if(user.isAdmin()==false) {
				res.status(401);
				res.header("Unauthorized","401");
				res.body("<b>401 Unauthorized: /rest/adminImageDelete User is not admin </b> ");
				res.type("text/html");	
				return null;
			}
			if(userView==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/adminImageDelete User not selected </b> ");
				res.type("text/html");	
				return null;
			}
			PostCommentBean postBean = g.fromJson(payload, PostCommentBean.class); //postId and a reason
			
			ResponseBean rsp=mainDao.userDao.adminImageDelete(postBean,userView.getId(),user.getId());
			return g.toJson(rsp);
				
		
		});
		
		
		
		post("/rest/postContent", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");
			
			if(user==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getMutualFriends User not logged </b> ");
				res.type("text/html");	
				return null;
			}
			PostContentBean contentBean = g.fromJson(payload, PostContentBean.class); //postId and a reason
			
		
		
			return g.toJson(mainDao.userDao.addContent(user,contentBean));
				
		
		});
		
		get("/rest/getUserView", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
		

			User userView=ss.attribute("userView");
			if(userView==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getUserView User not selected </b> ");
				res.type("text/html");	
				return null;
			}else {
				return g.toJson(mainDao.userDao.getUserInfo(userView));
			}
		
				
		
		});
		
		get("/rest/getUserViewPosts", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
			User user=ss.attribute("user");

			User userView=ss.attribute("userView");
			
			if(userView==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getUserViewPosts User not selected </b> ");
				res.type("text/html");	
				return null;
			}else {
				return g.toJson(mainDao.userDao.getUserPosts(userView));
			}
		
				
		
		});
		
		
		get("/rest/getUserViewPhotos", (req, res) -> {
			res.type("application/json");
			String payload = req.body();
			Session ss = req.session(true);
		

			User userView=ss.attribute("userView");
			if(userView==null) {
				res.status(400);
				res.header("Bad Request","400");
				res.body("<b>400 Bad request: /rest/getUserViewPhotos User not selected </b> ");
				res.type("text/html");	
				return null;
			}else {
				return g.toJson(mainDao.userDao.getUserPhotos(userView));
			}
		
				
		
		});
	}
	
	
	


	private static Map<String, Object> root;
	private static Configuration cfg;
	
	private static void initFtl() throws IOException {
		cfg = new Configuration(Configuration.VERSION_2_3_29);
		cfg.setDefaultEncoding("UTF-8");
		root = new HashMap<String, Object>();
		root.put("title", "FreeMarker template engine example");
		
		cfg.setDirectoryForTemplateLoading(new File("./templates"));		
	}

}
