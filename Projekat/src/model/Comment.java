package model;

import java.util.Date;

public class Comment {
	private int id;
	private int userId;
	private String text;
	private Date postingDate; 
	private boolean isEdited;
	private Date editingDate;
	private boolean isDeleted;
	
	
	public Comment() {}


	public Comment(int id, int userId, String text, Date postingDate, boolean isEdited, Date editingDate,
			boolean isDeleted) {
		super();
		this.id = id;
		this.userId = userId;
		this.text = text;
		this.postingDate = postingDate;
		this.isEdited = isEdited;
		this.editingDate = editingDate;
		this.isDeleted = isDeleted;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getPostingDate() {
		return postingDate;
	}


	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}


	public boolean isEdited() {
		return isEdited;
	}


	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}


	public Date getEditingDate() {
		return editingDate;
	}


	public void setEditingDate(Date editingDate) {
		this.editingDate = editingDate;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	@Override
	public String toString() {
		return "Comment [id=" + id + ", userId=" + userId + ", text=" + text + ", postingDate=" + postingDate
				+ ", isEdited=" + isEdited + ", editingDate=" + editingDate + ", isDeleted=" + isDeleted + "]";
	}
	
	
}
