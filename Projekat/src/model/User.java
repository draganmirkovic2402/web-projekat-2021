package model;


import java.util.ArrayList;
import java.util.Date;


public class User  {
	
	private int id;
	private String username;
	private String password;
	private String email;
	private String name;
	private String lastname;
	
	private Date dob ;
	private String gender;
	private boolean isAdmin;
	private int profilePicId;
	private ArrayList<Integer> postsIds;
	private ArrayList<Integer> imagesIds;
	private ArrayList<Integer> frndReqIds;
	private ArrayList<Integer> frndIds;  //list of friends usernames
	private boolean isPrivate;
	private boolean isBlocked;
	
	
	
	public User(int id,String username, String password, String email, String name, String lastname, Date dob, String gender,
			boolean isAdmin, int profilePic, boolean isPrivate,boolean isBlocked) {
		super();
		this.id=id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.lastname = lastname;
		this.dob = dob;
		this.gender = gender;
		this.isAdmin = isAdmin;
		this.profilePicId = profilePic;
		this.postsIds = new ArrayList<Integer>();
		this.imagesIds = new ArrayList<Integer>();
		this.frndReqIds = new ArrayList<Integer>();
		this.frndIds = new ArrayList<Integer>();
		this.isPrivate = isPrivate;
		this.isBlocked=isBlocked;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getLastname() {
		return lastname;
	}



	public void setLastname(String lastname) {
		this.lastname = lastname;
	}



	public Date getDob() {
		return dob;
	}



	public void setDob(Date dob) {
		this.dob = dob;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public int getProfilePic() {
		return profilePicId;
	}



	public void setProfilePic(int profilePic) {
		this.profilePicId = profilePic;
	}



	public ArrayList<Integer> getPostsIds() {
		return postsIds;
	}



	public void setPostsIds(ArrayList<Integer> postsIds) {
		this.postsIds = postsIds;
	}

	public boolean addPost(int postId) {
		if (!this.postsIds.contains(postId)) {
			this.postsIds.add(postId);
			return true;
		}
		return false;
	}

	public ArrayList<Integer> getImages() {
		return imagesIds;
	}

	public boolean addImage(int imageIdP) {
		if (!this.imagesIds.contains(imageIdP)) {
			this.imagesIds.add(imageIdP);
			return true;
		}
		return false;
	}

	public void setImages(ArrayList<Integer> images) {
		this.imagesIds = images;
	}



	public ArrayList<Integer> getFrndReqIds() {
		return frndReqIds;
	}
	
	public boolean addFrbdReq(int frndReqIdP) {
		if (!this.frndReqIds.contains(frndReqIdP)) {
			this.frndReqIds.add(frndReqIdP);
			return true;
		}
		return false;
	}


	public void setFrndReqIds(ArrayList<Integer> frndReqIds) {
		this.frndReqIds = frndReqIds;
	}
    
	


	public ArrayList<Integer> getFrndIds() {
		return frndIds;
	}
	
	public boolean addFriend(int idPar) {
		if (!this.frndIds.contains(idPar)) {
			this.frndIds.add(idPar);
			return true;
		}
		return false;
	}


	public void setFrndIds(ArrayList<Integer> frndIds) {
		this.frndIds = frndIds;
	}



	public boolean isPrivate() {
		return isPrivate;
	}



	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}



	public boolean isAdmin() {
		return isAdmin;
	}
	



	public boolean isBlocked() {
		return isBlocked;
	}



	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}



	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email + ", name="
				+ name + ", lastname=" + lastname + ", dob=" + dob + ", gender=" + gender + ", isAdmin=" + isAdmin
				+ ", profilePic=" + profilePicId + ", postsIds=" + postsIds + ", images=" + imagesIds + ", frndReqIds="
				+ frndReqIds + ", frndIds=" + frndIds + ", isPrivate=" + isPrivate + ", isBlocked=" + isBlocked + "]";
	}





}
