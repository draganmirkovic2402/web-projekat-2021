package model;

public enum Status {
	ON_WAIT,
	ACCEPTED,
	REJECTED
}
