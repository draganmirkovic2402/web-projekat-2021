package model;

import java.util.Date;

public class DirectMessage {
	private int id;
	private int senderId;
	private int recieverId;
	private String text;
	private Date date;
	private boolean isDeleted;
	
	public DirectMessage() {}

	public DirectMessage(int id, int senderId, int recieverId, String text, Date date, boolean isDeleted) {
		super();
		this.id = id;
		this.senderId = senderId;
		this.recieverId = recieverId;
		this.text = text;
		this.date = date;
		this.isDeleted = isDeleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getRecieverId() {
		return recieverId;
	}

	public void setRecieverId(int recieverId) {
		this.recieverId = recieverId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "DirectMessage [id=" + id + ", senderId=" + senderId + ", recieverId=" + recieverId + ", text=" + text
				+ ", date=" + date + ", isDeleted=" + isDeleted + "]";
	}
	
	
	

}
