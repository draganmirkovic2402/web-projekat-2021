package model;

import java.util.ArrayList;
import java.util.Date;

public class Post {
	private int id;
	private String picturePath;
	private Date date;
	private String caption;
	private ArrayList<Integer> commentsIds;
	private boolean isDeleted;
	
	public Post() {}

	public Post(int id, String picturePath, Date date,String caption,boolean isDeleted) {
		super();
		this.id = id;
		this.picturePath = picturePath;
		this.date= date;
		this.caption = caption;
		this.isDeleted=isDeleted;
		this.commentsIds=new ArrayList<Integer>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public ArrayList<Integer> getCommentsIds() {
		return commentsIds;
	}

	public void setCommentsIds(ArrayList<Integer> commentsIds) {
		this.commentsIds = commentsIds;
	}
	
	public void addComment(int commentIdP) {
		this.commentsIds.add(commentIdP);
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", imageId=" + picturePath + ", date=" + date + ", caption=" + caption + ", commentsIds="
				+ commentsIds + ", isDeleted=" + isDeleted + "]";
	}

	
	
	
	
	
	
}
