package model;

import java.util.ArrayList;
import java.util.Date;

public class Image {
	private int id;
	private String picturePath;
	private Date date;
	private String caption;
	private ArrayList<Integer> commentsIds;
	private boolean isDeleted;
	
	public Image() {}

	public Image(int id, String picturePath, String caption, Date date,
			boolean isDeleted) {
		super();
		this.id = id;
		this.picturePath = picturePath;
		this.caption = caption;
		this.date = date;
		this.commentsIds = new ArrayList<Integer>();
		this.isDeleted = isDeleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ArrayList<Integer> getCommentsIds() {
		return commentsIds;
	}

	public void setCommentsIds(ArrayList<Integer> commentsIds) {
		this.commentsIds = commentsIds;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "Image [id=" + id + ", picturePath=" + picturePath + ", caption=" + caption + ", date=" + date
				+ ", commentsIds=" + commentsIds + ", isDeleted=" + isDeleted + "]";
	}

	

}
