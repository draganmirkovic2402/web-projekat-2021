package cfg;

public class Config {
	public static final String USERS_FILE="data/users.json";
	public static final String COMMENTS_FILE="data/comments.json";
	public static final String DIRECT_MESSAGES_FILE="data/direct_messages.json";
	public static final String FRIEND_REQUESTS_FILE="data/friend_requests.json";
	public static final String POSTS_FILE="data/posts.json";
	public static final String IMAGES_FILE="data/images.json";
	
	public static final String USERS_BACKUP_FILE="backup/data/users.json";
	public static final String COMMENTS_BACKUP_FILE="backup/data/comments.json";
	public static final String DIRECT_MESSAGES_BACKUP_FILE="backup/data/direct_messages.json";
	public static final String FRIEND_REQUESTS_BACKUP_FILE="backup/data/friend_requests.json";
	public static final String POSTS_BACKUP_FILE="backup/data/posts.json";
	public static final String IMAGES_BACKUP_FILE="backup/data/images.json";
	
	public static final String IMAGES_FOLDER="../images/";
	
	public Config() {}
	
	
	
}
