package dao;

import com.google.gson.JsonElement;

import beans.UserTableBean;
import cfg.Config;
import model.User;

public class MainDao {
	public UserDao userDao;
	public PostDao postDao;
	public ImageDao imageDao;
	public FriendRequestDao friendRequestDao;
	public DirectMessageDao directMessageDao;
	public CommentDao commentDao;
	
	public Config cfg;
	
	public MainDao() {
		this.userDao=new UserDao(cfg.USERS_FILE,cfg.USERS_BACKUP_FILE);
		this.postDao=new PostDao(cfg.POSTS_FILE,cfg.POSTS_BACKUP_FILE);
		this.imageDao=new ImageDao(cfg.IMAGES_FILE,cfg.IMAGES_BACKUP_FILE,cfg.IMAGES_FOLDER);
		this.friendRequestDao= new FriendRequestDao(cfg.FRIEND_REQUESTS_FILE,cfg.FRIEND_REQUESTS_BACKUP_FILE);
		this.directMessageDao=new DirectMessageDao(cfg.DIRECT_MESSAGES_FILE,cfg.DIRECT_MESSAGES_BACKUP_FILE);
		this.commentDao=new CommentDao(cfg.COMMENTS_FILE,cfg.COMMENTS_BACKUP_FILE);
		this.userDao.setFriendRequestDao(friendRequestDao);
		this.userDao.setDircetMessageDao(directMessageDao);
		this.userDao.setPostsDao(postDao);
		this.userDao.setCommentsDao(commentDao);
		this.userDao.setImageDao(imageDao);
	}
	
	
	public boolean loadData() {
		System.out.println("Loading data ...");
		boolean status=true;
		status=status && this.userDao.loadData();
		status=status && this.imageDao.loadData();
		status=status && this.friendRequestDao.loadData();
		status=status && this.directMessageDao.loadData();
		status=status && this.postDao.loadData();		
		status=status && this.commentDao.loadData();
		return status;		
	}
	
	public boolean createMockUpData(boolean createBackup) {
		boolean status=true;
		status=status && this.userDao.createMockUp(createBackup);
		status=status && this.imageDao.createMockUp(createBackup);
		status=status && this.friendRequestDao.createMockUp(createBackup);
		status=status && this.directMessageDao.createMockUp(createBackup);
		status=status && this.postDao.createMockUp(createBackup);	
		status=status && this.commentDao.createMockUp(createBackup);
		return status;
	}



	

}
