package dao;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import model.Image;
import model.Post;
import model.User;


public class PostDao {
	private ArrayList<Post> postsList;
	private  String FILE_PATH="";
	private  String BACKUP_FILE_PATH="";
	private Gson g;
	private int currentId;
	
	
	public PostDao() {
		this.postsList=new ArrayList<Post>();
		this.g=new Gson();
	}

	public PostDao(String postsFile, String postsBackupFile) {
		this.postsList=new ArrayList<Post>();
		this.FILE_PATH=postsFile;
		this.BACKUP_FILE_PATH=postsBackupFile;
		this.g=new Gson();
	}

	public ArrayList<Post> getPostsList() {
		return postsList;
	}

	public void setPostsList(ArrayList<Post> postsList) {
		this.postsList = postsList;
	}
	 
	public boolean addPost(Post postP) {
		if (!contains(postP.getId())) {
			this.postsList.add(postP);
			return true;
		}
		return false;
	}
	
	private boolean contains(int id) {
		boolean contains=false;
		for (Post post : postsList) 
			if (post.getId()==id) 
				contains=true;		
		return contains;
		
	}

	public boolean loadData() {
		try {
        	 Reader reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        	 Post[] postList = g.fromJson(reader,Post[].class);
        	 
        	 for (Post post : postList) {
				addPost(post);
			}
        	 reader.close();
        	 currentId=postsList.size();
        	 return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int getNextId() {
		currentId++;
		return currentId;
	}
	
	public boolean saveData(boolean toBackup) {
		boolean status=true;
		Path path = Paths.get(this.FILE_PATH);
        try (Writer writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement tree = gson.toJsonTree(this.postsList);
            gson.toJson(tree, writer);
            writer.close();
           	status=true;
        }catch (Exception e) {
			e.printStackTrace();
			status=false;
		}
        
        if (toBackup) {
        	Path backup_path = Paths.get(this.BACKUP_FILE_PATH);
            try (Writer writer = Files.newBufferedWriter(backup_path, StandardCharsets.UTF_8)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement tree = gson.toJsonTree(this.postsList);
                gson.toJson(tree, writer);
                writer.close();
               	status=true;
            }catch (Exception e) {
    			e.printStackTrace();
    			status=false;
    		}
			
		}
        return status;
		
	}
	
	
	public Post getPostById(int id ) {
		for (Post post : postsList) {
			if (post.getId()==id) {
				return post;
			}
		}
		return null;
	}
	

	public boolean createMockUp(boolean createBackup) {
		
		Post p1=new Post(1, "../images/postpic1.jpg",new Date(120, 0, 20, 12, 40, 4), "Kako lijep pejzas",false);
		Post p2=new Post(2,"../images/postpic2.jpg",new Date(120, 0, 20, 15, 40, 4),"Moreeeee",false);
		p1.addComment(1);
		p1.addComment(2);
		this.postsList.add(p1);
		this.postsList.add(p2);
		return saveData(createBackup);
	}
}
