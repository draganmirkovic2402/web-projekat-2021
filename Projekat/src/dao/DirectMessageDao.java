package dao;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import model.Comment;
import model.DirectMessage;
import model.FriendRequest;

public class DirectMessageDao {
	private ArrayList<DirectMessage> directMessagesList;
	private  String FILE_PATH="";
	private  String BACKUP_FILE_PATH="";
	private Gson g;
	private int currentId;
	
	public DirectMessageDao() {
		this.directMessagesList=new ArrayList<DirectMessage>();
		this.g=new Gson();
	}

	public DirectMessageDao(String directMessagesFile, String directMessagesBackupFile) {
		this.directMessagesList=new ArrayList<DirectMessage>();
		this.FILE_PATH=directMessagesFile;
		this.BACKUP_FILE_PATH=directMessagesBackupFile;
		this.g=new Gson();
	}

	public ArrayList<DirectMessage> getDirectMessagesList() {
		return directMessagesList;
	}

	public void setDirectMessagesList(ArrayList<DirectMessage> directMessagesList) {
		this.directMessagesList = directMessagesList;
	}
	
	public boolean addDirectMessage(DirectMessage directMessageP) {
		if (!contains(directMessageP.getId())) {
			this.directMessagesList.add(directMessageP);
			return true;
		}
		return false;
	}
	
	private boolean contains(int id) {
		boolean contains=false;
		for (DirectMessage dm : directMessagesList) 
			if (dm.getId()==id) 
				contains=true;		
		return contains;
		
	}

	public boolean loadData() {
		try {
        	 Reader reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        	 DirectMessage[] dmList = g.fromJson(reader,DirectMessage[].class);
        	 
        	 for (DirectMessage dm : dmList) {
				addDirectMessage(dm);
			}
        	 reader.close();
        	 currentId=directMessagesList.size();
        	 return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int getNextId() {
		currentId++;
		return currentId;
	}
	
	public boolean saveData(boolean toBackup) {
		boolean status=true;
		Path path = Paths.get(this.FILE_PATH);
        try (Writer writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement tree = gson.toJsonTree(this.directMessagesList);
            gson.toJson(tree, writer);
            writer.close();
           	status=true;
        }catch (Exception e) {
			e.printStackTrace();
			status=false;
		}
        
        if (toBackup) {
        	Path backup_path = Paths.get(this.BACKUP_FILE_PATH);
            try (Writer writer = Files.newBufferedWriter(backup_path, StandardCharsets.UTF_8)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement tree = gson.toJsonTree(this.directMessagesList);
                gson.toJson(tree, writer);
                writer.close();
               	status=true;
            }catch (Exception e) {
    			e.printStackTrace();
    			status=false;
    		}
			
		}
        return status;
		
	}

	public boolean createMockUp(boolean createBackup) {
		DirectMessage dm1=new DirectMessage(1, 1, 2, "Pozdrav drugi admine evo stize poruka za vas", new Date(120, 0, 19, 18, 25, 23), false);
		DirectMessage dm2=new DirectMessage(2, 2, 1, "Pozdrav prvi admine evo stize vam odgovor", new Date(120, 0, 19, 19, 45, 23), false);
		this.directMessagesList.add(dm1);
		this.directMessagesList.add(dm2);
		return saveData(createBackup);
	}
	

}
