package dao;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import beans.ChatBean;
import beans.CommentBean;
import beans.FriendRequestBean;
import beans.ImageBean;
import beans.LoginBean;
import beans.MessageBean;
import beans.PostBean;
import beans.PostCommentBean;
import beans.PostContentBean;
import beans.RequestHandlerBean;
import beans.ResponseBean;
import beans.SearchBean;
import beans.SignUpBean;
import beans.TextBean;
import beans.UserEditBean;
import beans.UserTableBean;
import beans.UsernameBean;
import javaxt.utils.string;
import model.Comment;
import model.DirectMessage;
import model.FriendRequest;
import model.Image;
import model.Post;
import model.Status;
import model.User;

public class UserDao {
	private ArrayList<User> usersList;
	private  String FILE_PATH="";
	private  String BACKUP_FILE_PATH="";
	private Gson g;
	private int currentId;
	private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy.");
	private FriendRequestDao friendRequestDao;
	private DirectMessageDao directMessageDao;
	private PostDao postsDao;
	private CommentDao commentsDao;
	private ImageDao imagesDao;
	
	public UserDao() {
		this.usersList=new ArrayList<User>();
		this.g=new Gson();
	}

	public UserDao(String usersFile, String usersBackupFile) {
		this.usersList=new ArrayList<User>();
		this.g=new Gson();
		this.FILE_PATH=usersFile;
		this.BACKUP_FILE_PATH=usersBackupFile;
	}

	public ArrayList<User> getUserList() {
		return usersList;
	}
	
	public void setFriendRequestDao(FriendRequestDao dao) {
		this.friendRequestDao=dao;
	}
	
	public void setImageDao(ImageDao dao) {
		this.imagesDao=dao;
	}
	
	public void setCommentsDao(CommentDao dao) {
		this.commentsDao=dao;
		
	}
	
	public void setDircetMessageDao(DirectMessageDao dao) {
		this.directMessageDao=dao;
	}
	
	public void setPostsDao(PostDao dao) {
		this.postsDao=dao;
	}
	
	public void setUserList(ArrayList<User> userList) {
		this.usersList = userList;
	}
	
	public boolean addUser(User userP) {
		if (!contains(userP.getId()) && !containsUsername(userP.getUsername()) ) {
			this.usersList.add(userP);
			return true;
		}
		return false;
	}
	
	private boolean containsUsername(String usernameP) {
		boolean contains=false;
		for (User user : usersList) {
			if (user.getUsername().equals(usernameP)) {
				contains=true;
			}
		}
		return contains;
	}
	
	private boolean contains(int id) {
		boolean contains=false;
		for (User us : usersList) 
			if (us.getId()==id) 
				contains=true;		
		return contains;
		
	}

	public boolean loadData() {
		try {
        	Reader reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        	User[] user2 = g.fromJson(reader,User[].class);
        	 
        	 for (User us : user2) {
				addUser(us);
			}
        	 reader.close();
        	 currentId=usersList.size();
        	 return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public int getNextId() {
		currentId++;
		return currentId;
	}
	
	public boolean saveData(boolean toBackup) {
		boolean status=true;
		Path path = Paths.get(this.FILE_PATH);
        try (Writer writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement tree = gson.toJsonTree(this.usersList);
            gson.toJson(tree, writer);
            writer.close();
           	status=true;
        }catch (Exception e) {
			e.printStackTrace();
			status=false;
		}
        
        if (toBackup) {
        	Path backup_path = Paths.get(this.BACKUP_FILE_PATH);
            try (Writer writer = Files.newBufferedWriter(backup_path, StandardCharsets.UTF_8)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement tree = gson.toJsonTree(this.usersList);
                gson.toJson(tree, writer);
                writer.close();
               	status=true;
            }catch (Exception e) {
    			e.printStackTrace();
    			status=false;
    		}
			
		}
        return status;
		
	}
	
	


	public boolean createMockUp(boolean createBackup) {
		
		User user1=new User(1, "gage", "gage2402", "gage@mail.com", "Dragan", "Mirkovic", new Date(100,1,24), "M", true, 1, true, false);
		User user2=new User(2, "aki", "kavasaki", "asi@mail.com", "Andrej", "Culjak", new Date(98,5,5), "M", true, 2, true, false);
		User user3=new User(3, "pero", "pero", "pero@mail.com", "Pero", "Peric", new Date(94,3,02), "M", false, 3, true, false);
		User user4=new User(4, "mika", "mika", "mika@mail.com", "Mika", "Mikic", new Date(94,5,22), "M", false, 4, true, false);
		user1.addFriend(2);
		user2.addFriend(1);
		user3.addFriend(4);
		user4.addFriend(3);
		
		user1.addImage(1);
		user1.addImage(5);
		user2.addImage(2);
		user2.addImage(6);
		user3.addImage(3);
		user4.addImage(4);
		
		user1.addFrbdReq(1);
		user2.addFrbdReq(2);
		
		user1.addPost(1);
		user2.addPost(2);
		
		this.usersList.add(user1);
		this.usersList.add(user2);
		this.usersList.add(user3);
		this.usersList.add(user4);
		return saveData(createBackup);
	}

	public ResponseBean login(LoginBean u) {
		ResponseBean rsp= new ResponseBean();
		
		for (User user : usersList) {
			if (user.getUsername().equals(u.getUsername()) && !user.isBlocked()) {
				if (user.getPassword().equals(u.getPassword())) {
					rsp.setMessage("Login successfull. Welcome: "+user.getName()+" "+user.getLastname());
					rsp.setCode(1);
					return rsp;
				}else {
					rsp.setMessage("Invalid password");
					rsp.setCode(3);
					return rsp;
				}
			}
		}
		rsp.setMessage("Username does not exist.");
		rsp.setCode(2);
		return rsp;
	}
	
	private boolean isAlpha(String name) {
	    return name.matches("[a-zA-Z]+");
	}
	
	public static boolean isValidEmail(String email)
	{
	String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
	Pattern pattern = Pattern.compile(regex);
	if (email == null)
	return false;
	return pattern.matcher(email).matches();
	}
	

	
	public ResponseBean signUp(SignUpBean u) throws ParseException {
		Date dob;
		if(isValidDate(u.getDob())) {
			try {
				dob=getDate(u.getDob());
				
				boolean isOld=isOld(dob);
				
			
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		
		ResponseBean rsp=new ResponseBean();
		if(u.getName()==null || u.getName().equals("")) {
			rsp.setMessage("Please enter name.");
			rsp.setCode(4);
			return rsp;
		}else if(!isAlpha(u.getName())) {
			rsp.setMessage("Name must contain only letters.");
			rsp.setCode(5);
			return rsp;
		}else if(u.getLastname()==null || u.getLastname().equals("")) {
			rsp.setMessage("Please enter lastname.");
			rsp.setCode(6);
			return rsp;
		}
		else if(!isAlpha(u.getLastname())) {
			rsp.setMessage("Lastname must contain only letters.");
			rsp.setCode(7);
			return rsp;
		}else if(u.getEmail()==null || u.getEmail().equals("")) {
			rsp.setMessage("Please enter email.");
			rsp.setCode(14);
			return rsp;
		}
		else if(!isValidEmail(u.getEmail())) {
			rsp.setMessage("Email must be valid.");
			rsp.setCode(15);
			return rsp;
		}
		else if(u.getUsername()==null || u.getUsername().equals("")) {
			rsp.setMessage("Please enter username.");
			rsp.setCode(8);
			return rsp;
		}
		else if(!isNumOrLetter(u.getUsername())) {
			rsp.setMessage("Username must contain only letters and numbers.");
			rsp.setCode(9);
			return rsp;
		}else if(u.getPassword()==null || u.getPassword().equals("")) {
			rsp.setMessage("Please enter password.");
			rsp.setCode(10);
			return rsp;
		}
		else if(!isNumOrLetter(u.getPassword())) {
			rsp.setMessage("Password must contain only letters and numbers.");
			rsp.setCode(11);
			return rsp;
		}else if(u.getPasswordConfirm()==null || u.getPasswordConfirm().equals("")) {
			rsp.setMessage("Please confirm password.");
			rsp.setCode(12);
			return rsp;
		}
		else if(!u.getPassword().equals(u.getPasswordConfirm())) {
			rsp.setMessage("Passwords must match.");
			rsp.setCode(13);
			return rsp;
		}
		else if(!isValidDate(u.getDob())) {
			rsp.setMessage("Please select DOB");
			rsp.setCode(16);
			return rsp;
		}else if(!isOld(getDate(u.getDob()))) {
			rsp.setMessage("You must be 18y or older to register");
			rsp.setCode(17);
			return rsp;
		}else if(u.getGender()==null) {
			rsp.setMessage("Select gender");
			rsp.setCode(18);
			return rsp;
		}else if(containsUsername(u.getUsername())) {
			rsp.setMessage("Username already exists");
			rsp.setCode(19);
			return rsp;
		}
		dob=getDate(u.getDob());
		String gender;
		if (u.getGender().equals("male")) {
			gender="M";
		}else {
			gender="F";
		}
		User user=new User(getNextId(), u.getUsername(), u.getPassword(), u.getEmail(), u.getName(), u.getLastname(), dob,gender, false, 0, false, false);
		
		this.usersList.add(user);
		this.saveData(false);
		
		rsp.setMessage("Successfull registration.");
		rsp.setCode(1);
		return rsp;
	}

	private boolean isOld(Date dob) {		
		if (dob.getYear()<=103) {
			return true;
		}else {
			return false;
		}
	}

	private Date getDate(String dob) throws ParseException {
		  DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    formatter.setLenient(false);
		    return formatter.parse(dob);
	}

	private boolean isValidDate(String dob) {
		Date date=null;
		
		try {
		    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    formatter.setLenient(false);
		    date = formatter.parse(dob);
		    
		    return true;
		} catch (Exception e) { 
		   
		    return false;
		}
	}

	private boolean isNumOrLetter(String username) {
		return username.matches("[a-zA-Z0-9]+");
	}

	public User getUserByUsername(String username) {
		for (User user : usersList) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		return null;
	}
	
	
	public Collection<UserTableBean> getUserFriendsTable(User mainUser){
		ArrayList<UserTableBean> friendsList=new ArrayList<UserTableBean>();
		if (mainUser==null) {
			return friendsList;
		}
		for (User friendUser : this.usersList) {
			if (mainUser.getFrndIds().contains(friendUser.getId()) && !friendUser.isBlocked()) {
				
				String dateString = formatter.format(friendUser.getDob());
				
				UserTableBean userBean= new UserTableBean(this.imagesDao.getImagePathById(friendUser.getProfilePic()),friendUser.getName(),friendUser.getLastname(),friendUser.getUsername(),friendUser.getEmail(),dateString,friendUser.getGender(),friendUser.isPrivate(),friendUser.isAdmin(),friendUser.isBlocked());
				friendsList.add(userBean);
				
			}
		}
		
		return friendsList;
		
	
	}
	
	public UserEditBean getUserEditData(User user) {
		UserEditBean userEditInfo=new UserEditBean();
		if (user==null) {
			return userEditInfo;
		}
		
		userEditInfo.setPicturePath(this.imagesDao.getImagePathById(user.getProfilePic()));
		userEditInfo.setName(user.getName());
		userEditInfo.setLastname(user.getLastname());
		userEditInfo.setEmail(user.getEmail());
		userEditInfo.setPassword(user.getPassword());
		userEditInfo.setPasswordConfirm("");
		userEditInfo.setGender(user.getGender());
		userEditInfo.setIsPrivate(user.isPrivate());
		String dateString = formatter.format(user.getDob());
		userEditInfo.setDob(dateString);
		return userEditInfo;
		
	}

	public UserTableBean getUserInfo(User user) {
		UserTableBean userInfo=new UserTableBean();
		if (user==null) {
			return userInfo;
		}
		userInfo.setPicturePath(this.imagesDao.getImagePathById(user.getProfilePic()));
		userInfo.setName(user.getName());
		userInfo.setLastname(user.getLastname());
		userInfo.setEmail(user.getEmail());
		userInfo.setUsername(user.getUsername());
		userInfo.setPrivate(user.isPrivate());
		userInfo.setGender(user.getGender());
		userInfo.setAdmin(user.isAdmin());
		String dateString = formatter.format(user.getDob());
		userInfo.setDob(dateString);
		return userInfo;
	}

	public ResponseBean editUserData(UserEditBean u,String username) throws ParseException {
		ResponseBean rsp=new ResponseBean();
		boolean changeDate=true;
		if (u.getDob()==null) 
			changeDate=false;
		
		if(u.getName()==null || u.getName().equals("")) {
			rsp.setMessage("Please enter name.");
			rsp.setCode(4);
			return rsp;
		}else if(!isAlpha(u.getName())) {
			rsp.setMessage("Name must contain only letters.");
			rsp.setCode(5);
			return rsp;
		}else if(u.getLastname()==null || u.getLastname().equals("")) {
			rsp.setMessage("Please enter lastname.");
			rsp.setCode(6);
			return rsp;
		}
		else if(!isAlpha(u.getLastname())) {
			rsp.setMessage("Lastname must contain only letters.");
			rsp.setCode(7);
			return rsp;
		}else if(u.getEmail()==null || u.getEmail().equals("")) {
			rsp.setMessage("Please enter email.");
			rsp.setCode(14);
			return rsp;
		}
		else if(!isValidEmail(u.getEmail())) {
			rsp.setMessage("Email must be valid.");
			rsp.setCode(15);
			return rsp;
		}else if(u.getPassword()==null || u.getPassword().equals("")) {
			rsp.setMessage("Please enter password.");
			rsp.setCode(10);
			return rsp;
		}
		else if(!isNumOrLetter(u.getPassword())) {
			rsp.setMessage("Password must contain only letters and numbers.");
			rsp.setCode(11);
			return rsp;
		}else if(u.getPasswordConfirm()==null || u.getPasswordConfirm().equals("")) {
			rsp.setMessage("Please confirm password.");
			rsp.setCode(12);
			return rsp;
		}
		else if(!u.getPassword().equals(u.getPasswordConfirm())) {
			rsp.setMessage("Passwords must match.");
			rsp.setCode(13);
			return rsp;
		}
		if (changeDate) {
			if(!isValidDate(u.getDob())) {
				rsp.setMessage("Please select DOB");
				rsp.setCode(16);
				return rsp;
			}else if(!isOld(getDate(u.getDob()))) {
				rsp.setMessage("You must be 18y or older");
				rsp.setCode(17);
				return rsp;
			}
			
		}
		
		User user=this.getUserByUsername(username);
		user.setName(u.getName());
		
		user.setLastname(u.getLastname());
		user.setEmail(u.getEmail());
		if (changeDate) {
			Date dob=getDate(u.getDob());
			user.setDob(dob);
		}
		user.setPassword(u.getPassword());
		saveData(false);
		rsp.setCode(1);
		rsp.setMessage("Edit succesfully saved");
		return rsp;
	}

	private boolean removeFriendId(User userP,int id) {
		for(int i=0; i<userP.getFrndIds().size();i++) {
			int idLista=userP.getFrndIds().get(i);
			if (idLista==id) {
				userP.getFrndIds().remove(i);
				return true;
			}
		}
		return false;
	}
	
	public ResponseBean removeFriend(String mainUser, String friendUser) {
		ResponseBean rsp=new ResponseBean();
		User userMain=getUserByUsername(mainUser);
		
		User userFriend=getUserByUsername(friendUser);

		if (userMain.getFrndIds().contains(userFriend.getId()) && userFriend.getFrndIds().contains(userMain.getId())) {
			
			if(removeFriendId(userMain, userFriend.getId()) && removeFriendId(userFriend, userMain.getId())) {
			rsp.setCode(1);
			rsp.setMessage("Friend successfully removed.");
			return rsp; 	
			}else {
				rsp.setCode(-3);
				rsp.setMessage("Error");
				return rsp;
			}
		}
		saveData(false);
		rsp.setCode(-2);
		rsp.setMessage("Error");
		return rsp;
	}

	public Collection<UserTableBean> getOtherUserTable(User userP) {
		ArrayList<UserTableBean> otherUsersList=new ArrayList<UserTableBean>();
		for (User user : this.usersList) {
			String dateString = formatter.format(user.getDob());
			
			UserTableBean userTable=new UserTableBean(this.imagesDao.getImagePathById(user.getProfilePic()), user.getName(), user.getLastname(), user.getUsername(), user.getEmail(), dateString, user.getGender(), user.isPrivate(),user.isAdmin(),user.isBlocked());
			if (userP==null) {
				otherUsersList.add(userTable);
				
			}else if(!userP.getUsername().equals(user.getUsername()) && !userP.getFrndIds().contains(user.getId())) {
				otherUsersList.add(userTable);
			}else {
				continue;
			}
			
		}
		return otherUsersList;
	}
	
	
	public Collection<UserTableBean> getFilteredOtherUsers(User userP,SearchBean srchBean) throws ParseException{
		ArrayList<UserTableBean> otherUserList=new ArrayList<UserTableBean>();
		for (User user : this.usersList) {
			String dateString = formatter.format(user.getDob());			
			UserTableBean userTable=new UserTableBean(this.imagesDao.getImagePathById(user.getProfilePic()), user.getName(), user.getLastname(), user.getUsername(), user.getEmail(), dateString, user.getGender(), user.isPrivate(),user.isAdmin(),user.isBlocked());
			boolean isGood=true;
			
			if (srchBean.getSearchName()!=null && !srchBean.getSearchName().equals("")) {
				if(!user.getName().toLowerCase().contains(srchBean.getSearchName().toLowerCase()))
					isGood=false;
			}
			if(srchBean.getSearchLastname()!=null && !srchBean.getSearchLastname().equals("")) {
				if(!user.getLastname().toLowerCase().contains(srchBean.getSearchLastname().toLowerCase()))
					isGood=false;
			}
			if(srchBean.getDobFrom()!=null) {
				if(isValidDate(srchBean.getDobFrom())) {
					Date dobFrom=getDate(srchBean.getDobFrom());
					if (user.getDob().compareTo(dobFrom)<0) {
						isGood=false;
					}
				}
			}
			if(srchBean.getDobTo()!=null) {
				if(isValidDate(srchBean.getDobTo())) {
					Date dobTo=getDate(srchBean.getDobTo());
					if (user.getDob().compareTo(dobTo)>0) {
						isGood=false;
					}
				}
			}
			if(userP==null && isGood) {
				otherUserList.add(userTable);}
			else if(userP==null) {
				continue;
			}
			else if(!userP.getUsername().equals(user.getUsername()) && !userP.getFrndIds().contains(user.getId()) && isGood) {
				otherUserList.add(userTable);
			}
			else
				continue;
				
		}
		
		if (srchBean.getSortCrit()==0 || srchBean.getSortCrit()==1) {
			otherUserList.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
		}else if (srchBean.getSortCrit()==2) {
			otherUserList.sort((o1, o2) -> o1.getLastname().compareTo(o2.getLastname()));
		}else {
			Collections.sort(otherUserList);
			Collections.reverse(otherUserList);
		}
		
		if(srchBean.getSortType()==2) {
			Collections.reverse(otherUserList);
		}
		
		return otherUserList;
	}

	public ResponseBean addFriend(String senderUsername, String recieverUsername) {
		ResponseBean rsp=new ResponseBean();
		User senderUser=getUserByUsername(senderUsername);		
		User recieverUser=getUserByUsername(recieverUsername);
		
		for(FriendRequest frndReq : this.friendRequestDao.getFriendRequestsList()) {
			if (frndReq.getSenderId()==senderUser.getId() &&  frndReq.getRecieverId()==recieverUser.getId() && frndReq.isDeleted()==false) {
				if (frndReq.getStatus()==Status.ON_WAIT) {
					rsp.setMessage("Request already sent, STATUS: ON_WAIT");
					rsp.setCode(-4);
					return rsp;
				}else if(frndReq.getStatus()==Status.REJECTED) {
					rsp.setMessage("Old request rejected, sending a new one. NEW STATUS: ON_WAIT");
					rsp.setCode(-5);
					frndReq.setStatus(Status.ON_WAIT);
					return rsp;
				}else {
					rsp.setMessage("You are already friends. STATUS: ACCEPTED");
					rsp.setCode(1);
				}
			}
		};
		for(FriendRequest frndReq : this.friendRequestDao.getFriendRequestsList()) {
			if(frndReq.getSenderId()==recieverUser.getId() && frndReq.getRecieverId()==senderUser.getId() && frndReq.isDeleted()==false && frndReq.getStatus()==Status.ON_WAIT) {
				rsp.setMessage("This user already sent you a request");
				rsp.setCode(-6);
				return rsp;
			}
		}
		FriendRequest frndReq=new FriendRequest(this.friendRequestDao.getNextId(), senderUser.getId(), recieverUser.getId(), Status.ON_WAIT, new Date(), false);
		this.friendRequestDao.addFriendRequest(frndReq);
		this.friendRequestDao.saveData(false);
		saveData(false);
		rsp.setMessage("Successfully sent request");
		rsp.setCode(1);
		return rsp;		
		

	}

	public Collection<FriendRequestBean> getUserFriendRequests(User user) {
		
		ArrayList<FriendRequestBean> frndReqList=new ArrayList<FriendRequestBean>();
		for(FriendRequest frndReq: this.friendRequestDao.getFriendRequestsList()) {
			if(frndReq.getRecieverId()==user.getId() && frndReq.getStatus()==Status.ON_WAIT) {
				User sender=getUserById(frndReq.getSenderId());
				String frndReqDate = formatter.format(frndReq.getDate());
				
				FriendRequestBean frndReqBean= new FriendRequestBean(frndReq.getId(), sender.getName(), sender.getLastname(),frndReqDate,frndReq.getStatus());
				
				frndReqList.add(frndReqBean);
			}
		}
		return frndReqList;
	}

	public User getUserById(int userId) {
		for (User user : usersList) {
			if (user.getId()==userId) {
				return user;
			}
		}
		return null;
	}

	public ResponseBean handleRequest(User user, RequestHandlerBean req) {
		ResponseBean rsp=new ResponseBean();
		for(FriendRequest frndReq : this.friendRequestDao.getFriendRequestsList()) {
			if(frndReq.getId()==req.getId() && frndReq.getRecieverId()==user.getId() && frndReq.getStatus()==Status.ON_WAIT && !frndReq.isDeleted()) {
				if (req.isAccept()) {
					frndReq.setStatus(Status.ACCEPTED);
					User sender=getUserById(frndReq.getSenderId());
					sender.addFriend(user.getId());
					user.addFriend(sender.getId());
					rsp.setMessage("Friend request successfully accepted");
					rsp.setCode(1);
					return rsp;
				}else {
					frndReq.setStatus(Status.REJECTED);
					rsp.setMessage("Friend request successfully rejected");
					rsp.setCode(1);
					return rsp;
				}
			}
		}
		saveData(false);
		this.friendRequestDao.saveData(false);
		rsp.setMessage("Error");
		rsp.setCode(-7);
		return rsp;
	}

	public ResponseBean makePublic(User user,boolean makePublic) {
		ResponseBean rsp=new ResponseBean();
		if (makePublic) {
			user.setPrivate(false);
			rsp.setMessage("Successfully set your profile public");
			rsp.setCode(1);
			return rsp;
		}
		user.setPrivate(true);
		saveData(false);
		rsp.setMessage("Successfully set your profile private");
		rsp.setCode(1);
		return rsp;
	}

	public Collection<ChatBean> getUserChats(User userMain) {
		ArrayList<ChatBean> chats=new ArrayList<ChatBean>();
		for (User user : this.usersList ) {
			if(user.getId()==userMain.getId())
				continue;
			if(userMain.getFrndIds().contains(user.getId()) || user.isAdmin()) {
				ChatBean chatTemp=new ChatBean();
				chatTemp.setId(user.getId());
				chatTemp.setReciever(userMain.getId());
				chatTemp.setPicturePath(this.imagesDao.getImagePathById(user.getProfilePic()));
				chatTemp.setName(user.getName());
				chatTemp.setlastname(user.getLastname());
				chatTemp.setAdmin(user.isAdmin());;
				chatTemp.setMessages(getSortedMessages(userMain.getId(),user.getId()));
				chats.add(chatTemp);
				
			} 			
		}
		return chats;
	}

	private Collection<MessageBean> getSortedMessages(int reciever, int sender) {
		ArrayList<MessageBean> messages=new ArrayList<MessageBean>();
		for(DirectMessage message : this.directMessageDao.getDirectMessagesList()) {
			
			if(message.getRecieverId()==reciever && message.getSenderId()==sender && !message.isDeleted()) {
				String dateString = formatter.format(message.getDate());
				MessageBean msg=new MessageBean(reciever,message.getText(), dateString,true);
				messages.add(msg);
			}else if(message.getRecieverId()==sender && message.getSenderId()==reciever && !message.isDeleted()) {
				String dateString = formatter.format(message.getDate());
				MessageBean msg=new MessageBean(reciever,message.getText(), dateString,false);
				messages.add(msg);
			}
			
		}
		Collections.sort(messages);
		return messages;
	}

	public MessageBean sendMessage(User user, TextBean txtBean) {
		MessageBean msgBean=new MessageBean();
		DirectMessage msg=new DirectMessage(this.directMessageDao.getNextId(), txtBean.getSenderId(), txtBean.getRecieverId(), txtBean.getText(), new Date(), false);
		
		this.directMessageDao.addDirectMessage(msg);
		this.directMessageDao.saveData(false);
		msgBean.setReciever(txtBean.getRecieverId());
		msgBean.setText(msg.getText());
		msgBean.setFromSender(false);
		msgBean.setdate(formatter.format(new Date()));
		return msgBean;
	}

	public Collection<UserTableBean> getMutualFriends(String usernameMain, String usernameSecond) {
		User userMain=this.getUserByUsername(usernameMain);
		User userSecond=this.getUserByUsername(usernameSecond);
		ArrayList<UserTableBean> mutualList=new ArrayList<UserTableBean>();
		for(User userOther : this.usersList) {
			if (userOther.getFrndIds().contains(userMain.getId()) && userOther.getFrndIds().contains(userSecond.getId())) {
				String date=formatter.format(userOther.getDob());
				UserTableBean bean=new UserTableBean(this.imagesDao.getImagePathById(userOther.getProfilePic()), userOther.getName(), userOther.getLastname(), userOther.getUsername(), userOther.getEmail(), date, userOther.getGender(), userOther.isPrivate(),userOther.isAdmin(),userOther.isBlocked());
				mutualList.add(bean);
			}
		}
		
		return mutualList;
		
	}

	public Collection<PostBean> getUserPosts(User userView) {
		ArrayList<PostBean> posts=new ArrayList<PostBean>();
		
		for(Post post : this.postsDao.getPostsList()) {
			if (userView.getPostsIds().contains(post.getId()) && !post.isDeleted()) {
				String dateString=formatter.format(post.getDate());
				
				PostBean bean=new PostBean(post.getId(),post.getPicturePath(), dateString, post.getCaption(), getPostComments(post));		
				posts.add(bean);
			}
		}
		
		return posts;
	}

	private Collection<CommentBean> getPostComments(Post post) {
		ArrayList<CommentBean> comments=new ArrayList<CommentBean>();
		for(Comment comment : this.commentsDao.getCommentsList()) {
			if (post.getCommentsIds().contains(comment.getId()) && !comment.isDeleted()) {
				User user=this.getUserById(comment.getUserId());
				String date="";
				if (comment.isEdited()) {
					date=formatter.format(comment.getEditingDate());
				}else {
					date=formatter.format(comment.getPostingDate());
				}
				
				CommentBean commentBean= new CommentBean(comment.getId(), user.getUsername(),user.getName(), user.getLastname(), comment.getText(), date, comment.isEdited());
				comments.add(commentBean);
			}
		}
		return comments;
	}
	
	private Collection<CommentBean> getPictureComments(Image img) {
		ArrayList<CommentBean> comments=new ArrayList<CommentBean>();
		for(Comment comment : this.commentsDao.getCommentsList()) {
			if (img.getCommentsIds().contains(comment.getId()) && !comment.isDeleted()) {
				User user=this.getUserById(comment.getUserId());
				String date="";
				if (comment.isEdited()) {
					date=formatter.format(comment.getEditingDate());
				}else {
					date=formatter.format(comment.getPostingDate());
				}
				
				CommentBean commentBean= new CommentBean(comment.getId(), user.getUsername(),user.getName(), user.getLastname(), comment.getText(), date, comment.isEdited());
				comments.add(commentBean);
			}
		}
		return comments;
	}

	public CommentBean postComment(User user, PostCommentBean postcmntBean) {
		Comment comment=new Comment(this.commentsDao.getNextId(), user.getId(), postcmntBean.getText(), new Date(), false, null, false);
		this.commentsDao.getCommentsList().add(comment);
		this.commentsDao.saveData(false);
		Post post=this.postsDao.getPostById(postcmntBean.getPostId());
		post.getCommentsIds().add(comment.getId());
		this.postsDao.saveData(false);
		String dateString=formatter.format(comment.getPostingDate());
		CommentBean bean= new CommentBean(comment.getId(), user.getUsername(),user.getName(), user.getLastname(), comment.getText(), dateString, false);
		return bean;
	}

	public Collection<ImageBean> getUserPhotos(User userView) {
		ArrayList<ImageBean> imagesList=new ArrayList<ImageBean>();
		for(Image img : this.imagesDao.getImagesList()) {
			if (userView.getImages().contains(img.getId()) && !img.isDeleted()) {
				ImageBean imgBean=new ImageBean(img.getId(), img.getPicturePath(), formatter.format(img.getDate()), img.getCaption(), getPictureComments(img));
				imagesList.add(imgBean);
			}
		}
		return imagesList;
	}

	public CommentBean postCommentImage(User user, PostCommentBean postcmntBean) {
		Comment comment=new Comment(this.commentsDao.getNextId(), user.getId(), postcmntBean.getText(), new Date(), false, null, false);
		this.commentsDao.getCommentsList().add(comment);
		this.commentsDao.saveData(false);
		Image img=this.imagesDao.getImageById(postcmntBean.getPostId());
		img.getCommentsIds().add(comment.getId());
		this.imagesDao.saveData(false);
		String dateString=formatter.format(comment.getPostingDate());
		CommentBean bean= new CommentBean(comment.getId(), user.getUsername(),user.getName(), user.getLastname(), comment.getText(), dateString, false);
		return bean;
	}

	public ResponseBean setUserProfilePicture(User user, int imageId) {
		ResponseBean rsp=new ResponseBean();
		if(user.getProfilePic()==imageId) {
			rsp.setCode(1);
			rsp.setMessage("This picture is already your profile picture.");
			return rsp;
		}else {
			user.setProfilePic(imageId);
			rsp.setCode(1);
			rsp.setMessage("Profile picture set");
			saveData(false);
			return rsp;
		}
	}

	public ResponseBean deletePost(PostBean postBean) {
		ResponseBean rsp=new ResponseBean("Post deleted successfully",1);
		Post post=this.postsDao.getPostById(postBean.getId());
		post.setDeleted(true);
		for(CommentBean cmntBean: postBean.getComments()) {
			Comment cmnt=this.commentsDao.getCommentById(cmntBean.getId());
			cmnt.setDeleted(true);
		}
		this.postsDao.saveData(false);
		this.commentsDao.saveData(false);
		return rsp;
	}

	public ResponseBean deleteComment(PostCommentBean cmntBean) {
		ResponseBean rsp=new ResponseBean("Comment successfully deleted.", 1);
		Comment cmnt=this.commentsDao.getCommentById(cmntBean.getPostId()); //returns id of comment
		this.commentsDao.saveData(false);
		cmnt.setDeleted(true);
		return rsp;
	}

	public ResponseBean deleteImage(ImageBean imgBean) {
		ResponseBean rsp=new ResponseBean();
		Image img=this.imagesDao.getImageById(imgBean.getId());
		if (img==null) {
			rsp.setCode(-1);
			rsp.setMessage("Error");
			return rsp;
		}
		img.setDeleted(true);
		this.imagesDao.saveData(false);
		for(CommentBean cmntBean: imgBean.getComments()) {
			Comment cmnt= this.commentsDao.getCommentById(cmntBean.getId());
			if (cmnt==null) {
				rsp.setCode(-1);
				rsp.setMessage("Error");
				return rsp;
			}
			cmnt.setDeleted(true);
		}
		this.commentsDao.saveData(false);
		rsp.setCode(1);
		rsp.setMessage("Image successfully deleted");
		return rsp;
	}

	public ResponseBean adminDeletePost(PostCommentBean postBean,int recieverId,int senderId) {
		ResponseBean rsp=new ResponseBean();
		Post post=this.postsDao.getPostById(postBean.getPostId());
		this.postsDao.saveData(false);
		if(post==null) {
			rsp.setCode(-1);
			rsp.setMessage("Error");
			return rsp;
		}
		post.setDeleted(true);
		for(int ids: post.getCommentsIds()) {
			Comment cmnt= this.commentsDao.getCommentById(ids);
			if (cmnt==null) {
				rsp.setCode(-1);
				rsp.setMessage("Error");
				return rsp;
			}
			cmnt.setDeleted(true);
		}
		this.commentsDao.saveData(false);
		DirectMessage directMessage=new DirectMessage(this.directMessageDao.getNextId(), senderId, recieverId, postBean.getText(), new Date(), false);
		this.directMessageDao.addDirectMessage(directMessage);
		this.directMessageDao.saveData(false);
		rsp.setCode(1);
		rsp.setMessage("Post successfully deleted and reason sent");
		return rsp;
	}

	
	public ResponseBean adminImageDelete(PostCommentBean postBean, int recieverId, int senderId) {
		ResponseBean rsp=new ResponseBean();
		Image img=this.imagesDao.getImageById(postBean.getPostId());
		this.imagesDao.saveData(false);
		if(img==null) {
			rsp.setCode(-1);
			rsp.setMessage("Error");
			return rsp;
		}
		img.setDeleted(true);
		for(int ids: img.getCommentsIds()) {
			Comment cmnt= this.commentsDao.getCommentById(ids);
			if (cmnt==null) {
				rsp.setCode(-1);
				rsp.setMessage("Error");
				return rsp;
			}
			cmnt.setDeleted(true);
		}
		this.commentsDao.saveData(false);
		DirectMessage directMessage=new DirectMessage(this.directMessageDao.getNextId(), senderId, recieverId, postBean.getText(), new Date(), false);
		this.directMessageDao.addDirectMessage(directMessage);
		this.directMessageDao.saveData(false);
		rsp.setCode(1);
		rsp.setMessage("Image successfully deleted and reason sent");
		return rsp;
	}

	public ResponseBean blockUser(User userToBlock) {
		ResponseBean rsp=new ResponseBean("User blocked", 1);
		userToBlock.setBlocked(true);
		saveData(false);
		return rsp;
	}
	
	public ResponseBean unblockUser(User userToBlock) {
		ResponseBean rsp=new ResponseBean("User unblocked", 1);
		userToBlock.setBlocked(false);
		saveData(false);
		return rsp;
	}

	public Object addContent(User user, PostContentBean contentBean) {
	
		if (contentBean.isPost()) {
			Post post=new Post(this.postsDao.getNextId(), contentBean.getPath(), new Date(), contentBean.getCaption(),false);
			this.postsDao.getPostsList().add(post);
			this.postsDao.saveData(false);
			String date=formatter.format(post.getDate());
			user.getPostsIds().add(post.getId());
			PostBean bean=new PostBean(post.getId(), post.getPicturePath(), date, post.getCaption(), getPostComments(post));
			return bean;
		}else {
			Image img=new Image(this.imagesDao.getNextId(), contentBean.getPath(), contentBean.getCaption(), new Date(), false);
			this.imagesDao.getImagesList().add(img);
			this.imagesDao.saveData(false);
			user.getImages().add(img.getId());
			String date=formatter.format(img.getDate());
			ImageBean imgBean=new ImageBean(img.getId(), img.getPicturePath(), date, img.getCaption(), getPictureComments(img));
			return imgBean;
		}
	}

	public MessageBean getUserMessageBean(User user, TextBean txtBean) {
		MessageBean msg=new MessageBean();
		msg.setText(txtBean.getText());
		msg.setdate(formatter.format(new Date()));
		msg.setFromSender(false);
		return msg;
	}





	
}
