package dao;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import model.Comment;
import model.DirectMessage;

public class CommentDao {
	private ArrayList<Comment> commentsList;
	private  String FILE_PATH="";
	private  String BACKUP_FILE_PATH="";
	private Gson g;
	private int currentId;
	
	public CommentDao() {
		this.commentsList=new ArrayList<Comment>();
		this.g=new Gson();
	}

	public CommentDao(String commentsFile, String commentsBackupFile) {
		this.commentsList=new ArrayList<Comment>();
		this.FILE_PATH=commentsFile;
		this.BACKUP_FILE_PATH=commentsBackupFile;
		this.g=new Gson();
	}

	public ArrayList<Comment> getCommentsList() {
		return commentsList;
	}

	public void setCommentsList(ArrayList<Comment> commentsList) {
		this.commentsList = commentsList;
	}
	
	public boolean addComment(Comment commentP) {
		if(!contains(commentP.getId())) {
			this.commentsList.add(commentP);
			return true;
		}
		return false;
	}
	
	private boolean contains(int id) {
		boolean contains=false;
		for (Comment comment : commentsList) 
			if (comment.getId()==id) 
				contains=true;		
		return contains;
		
	}

	public boolean loadData() {
		try {
        	 Reader reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        	 Comment[] cmmList = g.fromJson(reader,Comment[].class);
        	 
        	 for (Comment com : cmmList) {
				addComment(com);
			}
        	 reader.close();
        	 currentId=commentsList.size();
        	 return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int getNextId() {
		currentId++;
		return currentId;
	}
	
	public boolean saveData(boolean toBackup) {
		boolean status=true;
		Path path = Paths.get(this.FILE_PATH);
        try (Writer writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement tree = gson.toJsonTree(this.commentsList);
            gson.toJson(tree, writer);
            writer.close();
           	status=true;
        }catch (Exception e) {
			e.printStackTrace();
			status=false;
		}
        
        if (toBackup) {
        	Path backup_path = Paths.get(this.BACKUP_FILE_PATH);
            try (Writer writer = Files.newBufferedWriter(backup_path, StandardCharsets.UTF_8)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement tree = gson.toJsonTree(this.commentsList);
                gson.toJson(tree, writer);
                writer.close();
               	status=true;
            }catch (Exception e) {
    			e.printStackTrace();
    			status=false;
    		}
			
		}
        return status;		
	}

	public boolean createMockUp(boolean createBackup) {
		Comment c1=new Comment(1, 3, "Slazem se", new Date(120, 0 , 21, 12, 00, 00), false, null, false);
		Comment c2=new Comment(2, 4, "WOW", new Date(120, 0, 21, 14, 00, 00), true, new Date(120, 0, 23, 12, 00, 5), false);
		this.commentsList.add(c1);
		this.commentsList.add(c2);
		return saveData(createBackup);
	}

	public Comment getCommentById(int id) {
		for (Comment comment : commentsList) {
			if (comment.getId()==id) {
				return comment;
			}
		}
		return null;
	}

}
