package dao;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import javaxt.utils.string;
import model.FriendRequest;
import model.Image;
import model.User;


public class ImageDao {
	private ArrayList<Image> imagesList;
	private  String FILE_PATH="";
	private  String BACKUP_FILE_PATH="";
	private String IMAGES_FOLDER="";
	private Gson g;
	private int currentId;
	
	public ImageDao() {
		this.imagesList=new ArrayList<Image>();
		this.g=new Gson();
	}


	public ImageDao(String imagesFile, String imagesBackupFile,String imagesFolder) {
		this.imagesList=new ArrayList<Image>();
		this.FILE_PATH=imagesFile;
		this.BACKUP_FILE_PATH=imagesBackupFile;
		this.IMAGES_FOLDER=imagesFolder;
		this.g=new Gson();
	}


	public ArrayList<Image> getImagesList() {
		return imagesList;
	}


	public void setImagesList(ArrayList<Image> imagesList) {
		this.imagesList = imagesList;
	}
	
	public boolean addImage(Image imageP) {
		if (!contains(imageP.getId())) {
			this.imagesList.add(imageP);
			return true;
		}
		return false;
	}
	
	private boolean contains(int id) {
		boolean contains=false;
		for (Image im : imagesList) 
			if (im.getId()==id) 
				contains=true;		
		return contains;
		
	}



	public boolean loadData() {
		try {
        	Reader reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        	 Image[] images = g.fromJson(reader,Image[].class);
        	 
        	 for (Image imageP : images) {
				addImage(imageP);
			}
        	 reader.close();
        	 currentId=imagesList.size();
        	 return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int getNextId() {
		currentId++;
		return currentId;
	}
	
	public boolean saveData(boolean toBackup) {
		boolean status=true;
		Path path = Paths.get(this.FILE_PATH);
        try (Writer writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement tree = gson.toJsonTree(this.imagesList);
            gson.toJson(tree, writer);
            writer.close();
           	status=true;
        }catch (Exception e) {
			e.printStackTrace();
			status=false;
		}
        
        if (toBackup) {
        	Path backup_path = Paths.get(this.BACKUP_FILE_PATH);
            try (Writer writer = Files.newBufferedWriter(backup_path, StandardCharsets.UTF_8)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement tree = gson.toJsonTree(this.imagesList);
                gson.toJson(tree, writer);
                writer.close();
               	status=true;
            }catch (Exception e) {
    			e.printStackTrace();
    			status=false;
    		}
			
		}
        return status;
		
	}
	
	public String getImagePathById(int id) {
		for (Image image : imagesList) {
			if(image.getId()==id)
				return image.getPicturePath();
		}
		return "../images/noPic.png";
	}


	public boolean createMockUp(boolean createBackup) {
		String path1=IMAGES_FOLDER+"pic1.jpg";
		String path2=IMAGES_FOLDER+"pic2.jpg";
		String path3=IMAGES_FOLDER+"pic3.jpg";
		String path4=IMAGES_FOLDER+"pic4.jpg";
		String path5=IMAGES_FOLDER+"postpic1.jpg";
		String path6=IMAGES_FOLDER+"postpic2.jpg";
		
		
		Image image1=new Image(1, "../images/pic1.jpg", "Profilna", new Date(120,0,15), false);
		Image image2=new Image(2, "../images/pic2.jpg", "Profilna", new Date(120,0,15), false);
		Image image3=new Image(3, "../images/pic3.jpg", "Profilna", new Date(120,0,15), false);
		Image image4=new Image(4, "../images/pic1.jpg", "Profilna", new Date(120,0,15), false);
		this.imagesList.add(image1);
		this.imagesList.add(image2);
		this.imagesList.add(image3);
		this.imagesList.add(image4);
		return saveData(createBackup);
	}


	public Image getImageById(int imgId) {
		for (Image image : imagesList) {
			if(image.getId()==imgId)
				return image;
		}
		return null;
	}
}
