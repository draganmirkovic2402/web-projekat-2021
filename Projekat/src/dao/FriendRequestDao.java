package dao;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import model.DirectMessage;
import model.FriendRequest;
import model.Post;
import model.Status;


public class FriendRequestDao {
	private ArrayList<FriendRequest> friendRequestsList;
	private  String FILE_PATH="";
	private  String BACKUP_FILE_PATH="";
	private Gson g;
	private int currentId;
	
	public FriendRequestDao() {
		this.friendRequestsList=new ArrayList<FriendRequest>();
		this.g=new Gson();
	}

	public FriendRequestDao(String friendRequestsFile, String friendRequestsBackupFile) {
		this.friendRequestsList=new ArrayList<FriendRequest>();
		this.FILE_PATH=friendRequestsFile;
		this.BACKUP_FILE_PATH=friendRequestsBackupFile;
		this.g=new Gson();
	}

	public ArrayList<FriendRequest> getFriendRequestsList() {
		return friendRequestsList;
	}

	public void setFriendRequestsList(ArrayList<FriendRequest> friendRequestsList) {
		this.friendRequestsList = friendRequestsList;
	}
	
	public boolean addFriendRequest(FriendRequest friendRequestP) {
		if (!contains(friendRequestP.getId())) {
			this.friendRequestsList.add(friendRequestP);
			return true;
		}
		return false;
	}
	
	private boolean contains(int id) {
		boolean contains=false;
		for (FriendRequest fr : friendRequestsList) 
			if (fr.getId()==id) 
				contains=true;		
		return contains;
		
	}

	public boolean loadData() {
		try {
        	 Reader reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        	 FriendRequest[] frList = g.fromJson(reader,FriendRequest[].class);
        	 
        	 for (FriendRequest fr : frList) {
				addFriendRequest(fr);
			}
        	 reader.close();
        	 currentId=friendRequestsList.size();
        	 return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int getNextId() {
		currentId++;
		return currentId;
	}
	
	public boolean saveData(boolean toBackup) {
		boolean status=true;
		Path path = Paths.get(this.FILE_PATH);
        try (Writer writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement tree = gson.toJsonTree(this.friendRequestsList);
            gson.toJson(tree, writer);
            writer.close();
           	status=true;
        }catch (Exception e) {
			e.printStackTrace();
			status=false;
		}
        
        if (toBackup) {
        	Path backup_path = Paths.get(this.BACKUP_FILE_PATH);
            try (Writer writer = Files.newBufferedWriter(backup_path, StandardCharsets.UTF_8)) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement tree = gson.toJsonTree(this.friendRequestsList);
                gson.toJson(tree, writer);
                writer.close();
               	status=true;
            }catch (Exception e) {
    			e.printStackTrace();
    			status=false;
    		}
			
		}
        return status;
		
	}

	public boolean createMockUp(boolean createBackup) {
		FriendRequest fr1=new FriendRequest(1, 3, 1, Status.ON_WAIT, new Date(120, 01, 20, 13, 20), false);
		FriendRequest fr2=new FriendRequest(2, 3, 2, Status.REJECTED, new Date(120, 01, 20, 13, 30), false);
		this.friendRequestsList.add(fr1);
		this.friendRequestsList.add(fr2);
		
		return saveData(createBackup);
	}

}
