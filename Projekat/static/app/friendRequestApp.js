


var friendRequestApp=new Vue({
	el: '#friendRequests',
	data:{		
		user:{},
		friendRequests: null,
		selectedFrReq: {},
		has: false,
		index:0		
	},
	mounted(){
		axios.get("../rest/getLoggedUserEditData").then(response => (this.user = response.data)),
		axios.get("../rest/getUserFriendRequests").then(response => (this.friendRequests = response.data,this.check()))
		
	},
	methods:{
		
		selectFrnReq: function(frReq,index){
			this.selectedFrReq=frReq;
			this.index=index;	
			
			
		},
		handleRequest: function(acceptP){
			if (confirm('Are you sure?') == true) {
			var data={id:this.selectedFrReq.id,accept:acceptP};
			axios.post("../rest/handleRequest",data).then(response => (toast(response.data.message)));
			Vue.delete(this.friendRequests,this.index);
			this.check();
			}
		},
		returnBack : function(){
			
			window.location.href = 'welcomePage.html';
			},
			
		check : function(){
			if(this.friendRequests.length==0){
				this.has=false;
			}else{
				this.has=true;
			}
		}
		
		
	}
})