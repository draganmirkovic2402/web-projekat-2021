

var messageApp=new Vue({
	el: '#messages',
	data:{
		userLoggedId: 1,
		chats: null,
		selectedChat:{},
		index:0,
		selected: false,
		text: null,
		connection:null
	},
	mounted(){
		axios.get("../rest/getLoggedUserId").then(response => (this.userLoggedId = response.data)),
		axios.get("../rest/getChats").then(response => (this.chats=response.data))
	},
	methods:{
		
		selectChat: function(chatP, index){
			this.selectedChat=chatP;
			
			this.index=index;
			this.selected=true;
			if( this.connection==null){
				this.connection=new WebSocket("ws://localhost:8080/ws");
			}
				
			this.connection.onmessage=function(event){		
			axios.post("../rest/sendMessage",event.data).then(response => message(response.data));
			};
			this.connection.onopen = function(event) {
		  
	      console.log(event)
	      console.log("Successfully connected to the echo websocket server...")
    };
	    	message=function(msg){
			
			if(msg.reciever===chatP.reciever){
			msg.isFromSender=true;			
			chatP.messages.push(msg);
			}
	}
		
		},
		sendMessage:  function(){
			
			var data={id:this.selectedChat.id, text: this.text};
			if(this.text===null){
				toast("Can not send empty message.");
			}else{
				axios.post("../rest/sendMessage",data).then(response => this.selectedChat.messages.push(response.data));
				this.text=null;				
			}
		},
		returnBack : function(){
			
			window.location.href = 'welcomePage.html';
			},
			
		sendMessage2: function() {
 		if(this.text===null){
				toast("Can not send empty message.");
			}else{
      	var text='{senderId:'+this.userLoggedId+', recieverId:'+this.selectedChat.id+',text:"'+this.text+'"}';
      	axios.post("../rest/sendMessageUser",text).then(response => this.selectedChat.messages.push(response.data));
      	this.connection.send(text);
      	this.text=null;
      	}
    }
 
	}
})