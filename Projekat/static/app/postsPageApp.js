



var postsPageApp=new Vue({
	el: "#posts",
	data: {
		imageToPost:null,
		imagePath: null,
		caption: null,
		imageUploaded:false,
		posting:false,
		logged:false,
		userLogged:{},
		userView:{},
		isMine:false,
		posts: null,
		hasPosts: false,
		selectedPost: {},
		selected:false,
		hasImage:false,
		index:0,
		text: null,
		reason:null,
		adminDeleting:false,
	},
	mounted(){
		axios.get("../rest/getLoggedUserInfo").then(response => (this.userLogged = response.data)),
		axios.get("../rest/getIsMine").then(response => (this.isMine=response.data)),
		axios.get("../rest/getUserView").then(response => (this.userView=response.data)),
		axios.get("../rest/getUserViewPosts").then(response => (this.posts=response.data, this.check())),
		axios.get("../rest/isLogged").then(response => (this.logged = response.data))

	
	},
	methods:{
		
		selectPost: function(post,index){
			this.selectedPost=post;
			this.index=index;
			
			this.selected=true;
			
			
			if(this.selectedPost.picturePath===undefined){
				this.hasImage=false;
			}else{
				this.hasImage=true;
			}
		},
		
				check : function(){
			if(this.posts.length==0){
				this.hasPosts=false;
			}else{
				this.hasPosts=true;
			}
		},
		
		comment: function(){
			var data={postId: this.selectedPost.id , text : this.text};
			axios.post("../rest/postComment",data).then(response => (this.selectedPost.comments.push(response.data)));
			this.text=null;
		},
		returnBack : function(){
			
			window.location.href = 'welcomePage.html';
			},
		deletePost: function(){
			
			axios.post("../rest/deletePost",this.selectedPost).then(response => toast(response.data.message));
			this.selected=false;
			this.hasImage=false;
			
			Vue.delete(this.posts,this.index);
			this.check();
		},
		deleteComment: function(id,index){
			
			data={postId:id,text:""}; //its actually comment id
			;
			axios.post("../rest/deleteComment",data).then(response => toast(response.data.message));
			Vue.delete(this.selectedPost.comments,index);		
		},
		adminDelete: function(){
			this.adminDeleting=true;
			if(this.reason===null){
				toast("Please provide a reason for deleting.");
			}else{
				var data={postId: this.selectedPost.id , text : this.reason};
				axios.post("../rest/adminPostDelete",data).then(response => toast(response.data.message));
				this.selected=false;
				this.hasImage=false;
				this.reason=null;
				this.adminDeleting=false;
				Vue.delete(this.posts,this.index);
				this.check();
			}
		},
		postPost:function(){
				this.posting=true;
				if(this.caption===null){
					toast("Please write a caption");
				}else{
					
					var data={path:this.imagePath,caption:this.caption,isPost:true};
					axios.post("../rest/postContent",data).then(response => (this.posts.push(response.data)));
					this.posting=false;
					this.imageUploaded=false;
					this.imagePath=null;
					this.caption=null;
					this.imageToPost=null;
					this.hasPosts=true;
					toast("Post added successfully.");
				}
			},
			onPickFile () {
	  this.$refs.fileInput.click();
	},
	onFilePicked (event) {
		  const files = event.target.files;
		  let filename = files[0].name;
		  const fileReader = new FileReader();
		  fileReader.addEventListener('load', () => {
		    this.imageUrl = fileReader.result
		  });
		  fileReader.readAsDataURL(files[0]);
		  this.imageToPost = files[0];
		  this.imagePath="../images/"+this.imageToPost.name;
		  console.log(this.imagePath);
		  this.imageUploaded=true;
	}
	}
})