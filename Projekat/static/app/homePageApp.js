


var homePageApp= new Vue({
	el: '#friends',
	data: {
		friends: null,
		mutualFriends: null,
		
		otherUsers: null,
		userLogged: {},
		logged: false,
		selectedUser: {},
		selected:false,
		hasFriends: false,
		hasExplore: false,
		isFriend: false,
		searchName: null,
		searchLastname: null,
		dobFrom: null,
		toastCount: 5,
		dobTo: null,
		sortCrit: null,
		
		critOptions:  [
      { text: 'By name', value: 1 },
      { text: 'By Lastname', value: 2 },
      { text: 'By Date of Birth', value: 3 }
      
    ],
		sortType: null,
		typeOptions:  [
      { text: 'Ascending', value: 1 },
      { text: 'Descending', value: 2 }
    ],
		index:5
	},
	mounted(){
		axios.get("../rest/isLogged").then(response => (this.logged = response.data)),
		axios.get("../rest/getFriends").then(response => (this.friends = response.data, this.checkFriends())),
		axios.get("../rest/getLoggedUserInfo").then(response => (this.userLogged = response.data)),
		axios.get("../rest/getOtherUsers").then(response=> (this.otherUsers = response.data, this.checkExplore()))
	
	},
	methods:{
		logout : function(){
			axios.get("../rest/logout");
			window.location.href = '../index.html';			
		},
		editProfile : function(){	
			window.location.href = 'editProfile.html';			
		},
		checkFriendRequests : function(){
			window.location.href = 'friendRequest.html';
		},
		checkMessages : function(){
			window.location.href = 'messagesPage.html';
		},
	
		
		selectUser: function(userP,isFriendP,indexP){
			
			this.selectedUser=userP;
			this.selected=true;
			this.isFriend=isFriendP;
			this.index=indexP;
			
			
			var data={username:this.selectedUser.username};
			if(this.logged==true){
				axios.post("../rest/getMutualFriends",data).then(response => this.mutualFriends=response.data );
				
			}
		} ,
		addFriend : function(){
			var data={username:this.selectedUser.username};
			axios.post("../rest/addFriend",data).then(response => (toast(response.data.message)));
		} ,
		sendMessage : function(){
			window.location.href = 'messagesPage.html';
		} ,
		viewPosts : function(){
			var data={username:this.selectedUser.username};
			axios.post("../rest/setViewUser",data);
			window.location.href = 'postsPage.html';
		} ,
		viewImages : function(){
			var data={username:this.selectedUser.username};
			axios.post("../rest/setViewUser",data);
			window.location.href = 'imagesPage.html';
		} ,
		viewMyPosts : function(){
			var data={username:this.userLogged.username};
			axios.post("../rest/setViewUser",data);
			window.location.href = 'postsPage.html';
		} ,
		viewMyImages : function(){
			var data={username:this.userLogged.username};
			axios.post("../rest/setViewUser",data);
			window.location.href = 'imagesPage.html';
		} ,
		hide : function(){
			this.selected=false;
		} ,
		
		removeFriend: function(){
			var data={username:this.selectedUser.username};
			axios.post("../rest/removeFriend",data).then(response => (toast(response.data.message)));
			
			this.otherUsers.push(this.friends[this.index]);
			Vue.delete(this.friends,this.index);
			this.checkFriends();
			this.hasExplore=true;
		} ,
		searchUsers: function(){
			var data={searchName:this.searchName , searchLastname:this.searchLastname, dobFrom:this.dobFrom, dobTo:this.dobTo,sortCrit:this.sortCrit,sortType:this.sortType };
			
			axios.post("../rest/getFilteredOtherUsers",data).then(response => (this.otherUsers = response.data));
		},
		blockUser(user,index){
			if (confirm('Are you sure?') == true) {
				user.isBlocked=true;
				axios.post("../rest/blockUser",user).then(response => toast(response.data.message));
			}
		},
		unblockUser(user,index){
			if (confirm('Are you sure?') == true) {
				user.isBlocked=false;
				axios.post("../rest/unblockUser",user).then(response => toast(response.data.message));
			}
		},
		
		checkFriends : function(){
			if(this.friends.length==0){
				this.hasFriends=false;
			}else{
				this.hasFriends=true;
			}
		},
		
		checkExplore : function(){
			if(this.otherUsers.length==0){
				this.hasExplore=false;
			}else{
				this.hasExplore=true;
			}
		},
		
		test: function(){
			
			axios.get("../rest/test")
		}
		  
    
		
	}

})