
var editProfileApp=new Vue({
	el: "#editProfile",
	data:{
		user:{},		
		date:null,
		response: {message:"Please fill up the form",code:0}
	},
	mounted(){
		axios.get("../rest/getLoggedUserEditData").then(response => (this.user = response.data))
	
	},
	methods:{
		
		saveData : function(){
			if (confirm('Are you sure?') == true) {
			console.log(this.user.name+" "+this.user.lastname+" "+this.user.email+" "+this.user.password+" "+this.user.passwordConfirm+" "+this.user.dob+" "+this.date);
			this.backUp=[this.user.name,this.user.lastname,this.user.email,this.user.password,this.user.dob];
			var data={picturePath:this.user.picturePath,name:this.user.name,lastname:this.user.lastname,email:this.user.email,password:this.user.password,passwordConfirm:this.user.passwordConfirm,dob:this.date};
			
			axios.post("../rest/saveData",data).then(response => (toast(response.data.message)));
			
			
		}},		
		returnBack : function(){
			if (confirm('Are you sure?') == true) {
			window.location.href = 'welcomePage.html';
			}
		},makePublic : function(){
			if (confirm('Are you sure?') == true) {
				axios.post("../rest/makePublic").then(response => (toast(response.data.message)));
				this.user.isPrivate=false;
			}
		},makePrivate : function(){
			if (confirm('Are you sure?') == true) {
				axios.post("../rest/makePrivate").then(response => (toast(response.data.message)));
				this.user.isPrivate=true;
			}
		},
		returnBack : function(){
			
			window.location.href = 'welcomePage.html';
			}
		
		
		
	},
	
})