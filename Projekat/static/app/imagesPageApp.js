



var imagesPageApp=new Vue({
	el: '#images',
	data: {
		imageToPost:null,
		imagePath: null,
		postingPictureCaption: null,
		imageUploaded:false,
		logged: false,
		userLogged:{},
		isMine:false,
		userView:{},
		images: null,
		hasImages: false,
		selectedImage: {},
		hasCaption: false,
		selected:false,
		index:0,
		text: null,
		reason: null,
		adminDeleting:false,
		posting: false
		
	},
	mounted(){
		axios.get("../rest/getLoggedUserInfo").then(response => (this.userLogged = response.data)),
		axios.get("../rest/getIsMine").then(response => (this.isMine=response.data)),
		axios.get("../rest/getUserView").then(response => (this.userView=response.data)),
		axios.get("../rest/getUserViewPhotos").then(response => (this.images=response.data, this.check())),
		axios.get("../rest/isLogged").then(response => (this.logged = response.data))
	
	},
	methods:{
		
		selectImage: function(photo,index){
			this.selectedImage=photo;
			this.index=index;
			this.selected=true;
			if(this.selectedImage.caption===null){
				this.hasCaption=false;
			}else{
				this.hasCaption=true;
			};
			
		},
		
		check : function(){
			
			if(this.images.length==0){
				this.hasImages=false;
			}else{
				this.hasImages=true;
			}
		},
		
		
		returnBack : function(){
			
			window.location.href = 'welcomePage.html';
			},
			comment: function(){
			var data={postId: this.selectedImage.id , text : this.text};
			axios.post("../rest/imageComment",data).then(response => (this.selectedImage.comments.push(response.data)));
			this.text=null;
		},
		setAsProfilePicture: function(){
			var data={postId: this.selectedImage.id , text : this.text};
			axios.post("../rest/setAsProfilePicture",data).then(response => toast(response.data.message));
			
		},
		deleteImage:function(){
			axios.post("../rest/deleteImage",this.selectedImage).then(response => toast(response.data.message));
			this.selected=false;
			this.hasCaption=false;
			Vue.delete(this.images,this.index);
			this.check();
		},
		deleteComment: function(id,index){
			
			data={postId:id,text:""}; //its actually comment id
			;
			axios.post("../rest/deleteComment",data).then(response => toast(response.data.message));
			Vue.delete(this.selectedImage.comments,index);		
		},
		adminDelete: function(){
			this.adminDeleting=true;
			if(this.reason===null){
				toast("Please provide a reason for deleting.");
			}else{
				var data={postId: this.selectedImage.id , text : this.reason};
				axios.post("../rest/adminImageDelete",data).then(response => toast(response.data.message));
				this.selected=false;
				this.hasCaption=false;
				this.reason=null;
				this.adminDeleting=false;
				Vue.delete(this.images,this.index);
				this.check();
			}
		},
			postImage:function(){
				this.posting=true;
				if(this.imageToPost===null){
					toast("Please upload a picture");
				}else{
					
					var data={path:this.imagePath,caption:this.postingPictureCaption,isPost:false};
					axios.post("../rest/postContent",data).then(response => (this.images.push(response.data)));
					this.posting=false;
					this.imageUploaded=false;
					this.imagePath=null;
					this.imageToPost=null;		
					this.postingPictureCaption=null;			
					toast("Image added successfully.");
					this.hasImages=true;
					
				}
			},
			onPickFile () {
	  this.$refs.fileInput.click();
	},
	onFilePicked (event) {
		  const files = event.target.files;
		  let filename = files[0].name;
		  const fileReader = new FileReader();
		  fileReader.addEventListener('load', () => {
		    this.imageUrl = fileReader.result
		  });
		  fileReader.readAsDataURL(files[0]);
		  this.imageToPost = files[0];
		  this.imagePath="../images/"+this.imageToPost.name;
		  
		  this.imageUploaded=true;
	}
		
	}
})
